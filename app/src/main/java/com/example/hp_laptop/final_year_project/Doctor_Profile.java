package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TimePicker;
import android.content.Intent;
import android.database.Cursor;
import android.drm.DrmStore;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.app.TimePickerDialog;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.scalified.fab.ActionButton;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;

import static android.app.Activity.RESULT_OK;

public class Doctor_Profile extends AppCompatActivity  {


    com.pkmmte.view.CircularImageView imageView;
    LinearLayout password_layout;
    EditText password;
    EditText re_password;
    EditText user_name;
    EditText user_phone_number;
    EditText user_email;
    LinearLayout only_docotor_layout=null;
    AutoCompleteTextView specialization=null;
    AutoCompleteTextView clinic_name_field=null;
    EditText cnic=null;
    EditText pmdc=null;
    EditText experinece=null;
    EditText clinic_contact_feild=null;
    static  Context context;

    ActionButton location_button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor__profile);
        Information.profile_photo=BitmapFactory.decodeResource(getResources(), R.drawable.profileicon);

        context=this;

//username,email,picture,phone
       imageView= (com.pkmmte.view.CircularImageView) findViewById(R.id.profilePic);
       user_name=(EditText) findViewById(R.id.user_name);
       user_name.setText("");
       user_phone_number=(EditText) findViewById(R.id.user_phone_number);
        user_phone_number.setText("");
       user_email=(EditText) findViewById(R.id.user_email);
        user_email.setText("");
       only_docotor_layout=(LinearLayout) findViewById(R.id.only_docotor_layout);
       only_docotor_layout.setVisibility(View.GONE);
        this.password_layout=(LinearLayout)findViewById(R.id.password_layout);
        this.password = (EditText) findViewById(R.id.Password);
        password.setText("");
        this.re_password = (EditText) findViewById(R.id.co_Password);
        re_password.setText("");
        location_button=(ActionButton) findViewById(R.id.location_button);

        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"select files"),456);
            }
        });



        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"select files"),456);
            }
        });


     final ActionButton cont =(ActionButton)findViewById(R.id.next);

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("check_value",deal_with_validations()+"");

                if(deal_with_validations()==true)
                {
                    /*Intent intent = new Intent(Doctor_Profile.this,user_sigin_location.class);
                    startActivity(intent);*/
                }

            }
        });



        // specilization fields
       specialization= (AutoCompleteTextView) findViewById(R.id.specilization);
       String [] a= getResources().getStringArray(R.array.specilizations);
       ArrayAdapter <String> ab= new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,a);
       specialization.setAdapter(ab);
        specialization.setText(" ");


 // clinic name field

        clinic_name_field=(AutoCompleteTextView) findViewById(R.id.Clinic_Name);
        String[] clinics = getResources().getStringArray(R.array.clinics);

        ArrayAdapter<String> ghi = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,clinics);
        clinic_name_field.setAdapter(ghi);
        clinic_name_field.setText("");

//pmdc/cnic
        cnic=(EditText)  findViewById(R.id.CNIC);
        cnic.setText("");
        pmdc=(EditText)  findViewById(R.id.PMDC);
        pmdc.setText("");
        //Experinece
        experinece=(EditText) findViewById(R.id.experience);
        experinece.setText("");

//clinic contact
        clinic_contact_feild=(EditText)  findViewById(R.id.Clinic_ContactNo);
        clinic_contact_feild.setText("");
//gender
        final RadioRealButtonGroup radiogroup= (RadioRealButtonGroup)findViewById(R.id.gender);
        radiogroup.setOnClickedButtonListener(new RadioRealButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(RadioRealButton button, int position) {
                Toast.makeText(Doctor_Profile.this, "Position: " + position, Toast.LENGTH_SHORT).show();

                if(position==1)
                {
                  Information.gender="male";
                }
                if(position==0)
                {
                   Information.gender="female";
                }
            }
        });

        final RadioRealButtonGroup radiogroup_1= (RadioRealButtonGroup)findViewById(R.id.proffesion);
        radiogroup_1.setOnClickedButtonListener(new RadioRealButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(RadioRealButton button, int position) {
                Toast.makeText(Doctor_Profile.this, "Position: " + position, Toast.LENGTH_SHORT).show();



                if(position==0)
                {
                    only_docotor_layout.setVisibility(View.GONE);
                    Information.users_Profession="patient";
                    Information.doctor_or_patient="patient";
                }

                if(position==1)
                {
                    only_docotor_layout.setVisibility(View.VISIBLE);
                    Information.users_Profession="doctor";
                    Information.doctor_or_patient="doctor";

                }



            }
        });





        final ActionButton next =(ActionButton)findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(deal_with_validations()==true)
                {
                    new Firebase_Actions().save_image(Doctor_Profile.this,user_email.getText().toString());
                    Information.current_loged_in_user_email=user_email.getText().toString();
                    Intent intent = new Intent(Doctor_Profile.this,user_sigin_location.class);
                    startActivity(intent);
                }




            }
        });

        if(Information.provider.equals("phone"))
        {
            user_phone_number.setText(Information.current_loged_in_user_email);
        }
        if(Information.provider.equals("google"))
        {
            user_email.setText(Information.current_loged_in_user_email);
        }

        }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK)
        {
            if(requestCode==456)
            {
                try {
                  Bitmap  photo = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
                    Information.selected_image_Uri=data.getData();;
                    imageView.setImageBitmap(photo);
                    Log.i("tictak", photo + "    photo       " + imageView);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }

   public  boolean deal_with_validations()
   {

       Log.d("user_email_1",user_email.getText().toString().isEmpty()+"   value");
       Log.d("user_email_2",user_email.getText().length()+"   value");

       if(user_name.getText().toString().isEmpty())
       {
           user_name.setError("Kindly enter Valid username");
           user_name.requestFocus();
           return false;
       }

       if(user_email.getText().toString().isEmpty() || user_email.getText().length()==0)
       {
           Log.d("user_email",user_email.getText().toString().isEmpty()+"    value");
           user_email.setError("Kindly enter Valid Email Id");
           user_email.requestFocus();
           return false;
       }
       if(password.getText().toString().isEmpty() || password.getText().length()==0)
       {
           Log.d("password",password.getText().toString().isEmpty()+"    value");
           password.setError("Kindly enter password");
           password.requestFocus();
           return false;
       }
       if(re_password.getText().toString().isEmpty() || re_password.getText().length()==0)
       {
           Log.d("re_password",re_password.getText().toString().isEmpty()+"    value");
           re_password.setError("Kindly enter Confrom password");
           re_password.requestFocus();
           return false;

       }
       if(user_phone_number.getText().toString().isEmpty() || user_phone_number.getText().length()==0)
       {
           Log.d("user_phone_number",user_phone_number.getText().toString().isEmpty()+"    value");
           user_phone_number.setError("Kindly enter vaid phonenumber");
           user_phone_number.requestFocus();
           return false;
       }

       else if(!password.getText().toString().equals(re_password.getText().toString()))
       {

           password.setError("your password and conform password are not same");
           password.requestFocus();
           return false;
       }
       else if(cnic.getText().toString().isEmpty() || cnic.getText().length()==0)
       {
           Log.d("cnic",cnic.getText().toString().isEmpty()+"    value");
           cnic.setError("Kindly enter Vali CNIC");
           cnic.requestFocus();
           return false;

       }





        if(Information.doctor_or_patient.equals("doctor"))
       {
           if(pmdc.getText().toString().isEmpty())
           {
               Log.d("pmdc",pmdc.getText().toString().isEmpty()+"    value");

               pmdc.setError("Kindly enter your PMDC number");
               pmdc.requestFocus();
               return false;
           }

           if(clinic_name_field.getText().toString().length()==0 || clinic_name_field.getText().length()==0)
           {
               Log.d("clinic_name_field",clinic_name_field.getText().toString().isEmpty()+"    value");
               clinic_name_field.setError("Kindly enter your Clinic Name");
               clinic_name_field.requestFocus();
               return false;
           }
           Toast.makeText(this,specialization.getText().toString().isEmpty()+"    value",Toast.LENGTH_LONG).show();
           if(specialization.getText().toString().length()==0 || specialization.getText().length()==0)
           {
               Log.d("specialization",specialization.getText().toString().isEmpty()+"    value");
               specialization.setError("Kindly select valid specialization");
               specialization.requestFocus();
               return false;
           }

           if(experinece.getText().toString().isEmpty() || experinece.getText().length()==0)
           {
               Log.d("experinece",experinece.getText().toString().isEmpty()+"    value");
               experinece.setError("Kindly select valid specialization");
               experinece.requestFocus();
               return false;
           }
           //clinic_contact_feild
           if(clinic_contact_feild.getText().toString().isEmpty() || clinic_contact_feild.getText().length()==0)
           {
               Log.d("specialization",specialization.getText().toString().isEmpty()+"    value");
               clinic_contact_feild.setError("Kindly enter clinic name");
               clinic_contact_feild.requestFocus();
               return false;
           }

        }

       Information.user_name=user_name.getText().toString();
       Information.users_email_id=user_email.getText().toString();
       Information.users_phone_number=user_phone_number.getText().toString();
       Information.cnic=cnic.getText().toString();

       if(Information.doctor_or_patient.equals("doctor"))
       {
           Information.pmdc = pmdc.getText().toString();
           Information.clinic_name = clinic_name_field.getText().toString();
           Information.specialization = specialization.getText().toString();
           Information.experience = experinece.getText().toString();
           Information.clnic_number = clinic_contact_feild.getText().toString();
       }


       return true;
   }

}
