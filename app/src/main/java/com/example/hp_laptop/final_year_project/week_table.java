package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class week_table extends Fragment {

    View view;
    String monday_list;
    String tuesday_list;
    String wednesday_list;
    String thursday_list;
    String friday_list;
    String saturday_list;
    String sunday_list;


    TextView mon_1,tue_1,wed_1,Th1_1,fri_1,sat_1,sun_1;
    TextView mon_2,tue_2,wed_2,Th1_2,fri_2,sat_2,sun_2;
    TextView mon_4,tue_4,wed_4,Th1_4,fri_4,sat_4,sun_4;
    TextView mon_5,tue_5,wed_5,Th1_5,fri_5,sat_5,sun_5;
    TextView mon_7,tue_7,wed_7,Th1_7,fri_7,sat_7,sun_7;
    TextView mon_8,tue_8,wed_8,Th1_8,fri_8,sat_8,sun_8;
    TextView mon_9,tue_9,wed_9,Th1_9,fri_9,sat_9,sun_9;
    TextView mon_10,tue_10,wed_10,Th1_10,fri_10,sat_10,sun_10;
    TextView mon_11,tue_11,wed_11,Th1_11,fri_11,sat_11,sun_11;
    TextView mon_12,tue_12,wed_12,Th1_12,fri_12,sat_12,sun_12;
    TextView mon_13,tue_13,wed_13,Th1_13,fri_13,sat_13,sun_13;
    TextView mon_14,tue_14,wed_14,Th1_14,fri_14,sat_14,sun_14;
    TextView mon_15,tue_15,wed_15,Th1_15,fri_15,sat_15,sun_15;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_week_table, container, false);

        download_time_table();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    public void download_time_table()
    {

        Log.d("check_time_table","check_time_table_out");

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("schedules").child((Information.selected_doctor.getUsers_email_id().replace(".","")).replace("@",""));
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot

                        Log.d("check_time_table","check_time_table_in");
                        Schedule schedule = dataSnapshot.getValue(Schedule.class);

                        monday_list=schedule.getMonday();
                        tuesday_list=schedule.getTuesday();
                        wednesday_list=schedule.getWednesday();
                        thursday_list=schedule.getThursday();
                        friday_list=schedule.getFriday();
                        saturday_list=schedule.getSaturday();
                        sunday_list=schedule.getSunday();
                        mark_days();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });

    }

    public void mark_days()
    {
        mon_1=(TextView)view.findViewById(R.id.mon_1);
        if(monday_list.contains("mond_1"))
        {
            mon_1.setBackgroundResource(R.drawable.filled_box);
        }

        tue_1=(TextView)view.findViewById(R.id.tue_1);
        if(tuesday_list.contains("tue_1"))
        {

        }

        wed_1=(TextView)view.findViewById(R.id.wed_1);
        if(wednesday_list.contains("wed_1"))
        {

        }

        Th1_1=(TextView)view.findViewById(R.id.Th1_1);
        if(thursday_list.contains("Th1_1"))
        {

        }

        fri_1=(TextView)view.findViewById(R.id.fri_1);
        if(friday_list.contains("fri_1"))
        {

        }

        sat_1=(TextView)view.findViewById(R.id.sat_1);
        if(saturday_list.contains("sat_1"))
        {

        }

        sun_1=(TextView)view.findViewById(R.id.sun_1);
        if(sunday_list.contains("sun_1"))
        {

        }

        // raow_2


        mon_2=(TextView)view.findViewById(R.id.mon_2);
        tue_2=(TextView)view.findViewById(R.id.tue_2);
        wed_2=(TextView)view.findViewById(R.id.wed_2);
        Th1_2=(TextView)view.findViewById(R.id.Th1_2);
        fri_2=(TextView)view.findViewById(R.id.fri_2);
        sat_2=(TextView)view.findViewById(R.id.sat_2);
        sun_2=(TextView)view.findViewById(R.id.sun_2);

        if(monday_list.contains("mon_2"))
        {
            mon_2.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_2"))
        {
            tue_2.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_2"))
        {
            wed_2.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_2"))
        {
            Th1_2.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_2"))
        {
            fri_2.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_2"))
        {
            sat_2.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_2"))
        {
            sun_2.setBackgroundResource(R.drawable.filled_box);
        }

        // row 3

        mon_4=(TextView)view.findViewById(R.id.mon_4);
        tue_4=(TextView)view.findViewById(R.id.tue_4);
        wed_4=(TextView)view.findViewById(R.id.wed_4);
        Th1_4=(TextView)view.findViewById(R.id.Th1_4);
        fri_4=(TextView)view.findViewById(R.id.fri_4);
        sat_4=(TextView)view.findViewById(R.id.sat_4);
        sun_4=(TextView)view.findViewById(R.id.sun_4);

        if(monday_list.contains("mon_4"))
        {
            mon_4.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_4"))
        {
            tue_4.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_4"))
        {
            wed_4.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_4"))
        {
            Th1_4.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_4"))
        {
            fri_4.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_4"))
        {
            sat_4.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_4"))
        {
            sun_4.setBackgroundResource(R.drawable.filled_box);
        }



        // row 4

        mon_5=(TextView)view.findViewById(R.id.mon_5);
        tue_5=(TextView)view.findViewById(R.id.tue_5);
        wed_5=(TextView)view.findViewById(R.id.wed_5);
        Th1_5=(TextView)view.findViewById(R.id.Th1_5);
        fri_5=(TextView)view.findViewById(R.id.fri_5);
        sat_5=(TextView)view.findViewById(R.id.sat_5);
        sun_5=(TextView)view.findViewById(R.id.sun_5);

        if(monday_list.contains("mon_5"))
        {
            mon_5.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_5"))
        {
            tue_5.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_5"))
        {
            wed_5.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_5"))
        {
            Th1_5.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_5"))
        {
            fri_5.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_5"))
        {
            sat_5.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_5"))
        {
            sun_5.setBackgroundResource(R.drawable.filled_box);
        }



        //

        mon_7=(TextView)view.findViewById(R.id.mon_7);
        tue_7=(TextView)view.findViewById(R.id.tue_7);
        wed_7=(TextView)view.findViewById(R.id.wed_7);
        Th1_7=(TextView)view.findViewById(R.id.Th1_7);
        fri_7=(TextView)view.findViewById(R.id.fri_7);
        sat_7=(TextView)view.findViewById(R.id.sat_7);
        sun_7=(TextView)view.findViewById(R.id.sun_7);

        if(monday_list.contains("mon_7"))
        {
            mon_7.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_7"))
        {
            tue_7.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_7"))
        {
            wed_7.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_7"))
        {
            Th1_7.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_7"))
        {
            fri_7.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_7"))
        {
            sat_7.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_7"))
        {
            sun_7.setBackgroundResource(R.drawable.filled_box);
        }

        //


        mon_8=(TextView)view.findViewById(R.id.mon_8);
        tue_8=(TextView)view.findViewById(R.id.tue_8);
        wed_8=(TextView)view.findViewById(R.id.wed_8);
        Th1_8=(TextView)view.findViewById(R.id.Th1_8);
        fri_8=(TextView)view.findViewById(R.id.fri_8);
        sat_8=(TextView)view.findViewById(R.id.sat_8);
        sun_8=(TextView)view.findViewById(R.id.sun_8);

        if(monday_list.contains("mon_8"))
        {
            mon_8.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_8"))
        {
            tue_8.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_8"))
        {
            wed_8.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_8"))
        {
            Th1_8.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_8"))
        {
            fri_8.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_8"))
        {
            sat_8.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_8"))
        {
            sun_8.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_9=(TextView)view.findViewById(R.id.mon_9);
        tue_9=(TextView)view.findViewById(R.id.tue_9);
        wed_9=(TextView)view.findViewById(R.id.wed_9);
        Th1_9=(TextView)view.findViewById(R.id.Th1_9);
        fri_9=(TextView)view.findViewById(R.id.fri_9);
        sat_9=(TextView)view.findViewById(R.id.sat_9);
        sun_9=(TextView)view.findViewById(R.id.sun_9);

        if(monday_list.contains("mon_9"))
        {
            mon_9.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_9"))
        {
            tue_9.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_9"))
        {
            wed_9.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_9"))
        {
            Th1_9.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_9"))
        {
            fri_9.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_9"))
        {
            sat_9.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_9"))
        {
            sun_9.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_10=(TextView)view.findViewById(R.id.mon_10);
        tue_10=(TextView)view.findViewById(R.id.tue_10);
        wed_10=(TextView)view.findViewById(R.id.wed_10);
        Th1_10=(TextView)view.findViewById(R.id.Th1_10);
        fri_10=(TextView)view.findViewById(R.id.fri_10);
        sat_10=(TextView)view.findViewById(R.id.sat_10);
        sun_10=(TextView)view.findViewById(R.id.sun_10);

        if(monday_list.contains("mon_10"))
        {
            mon_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_10"))
        {
            tue_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_10"))
        {
            wed_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_10"))
        {
            Th1_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_10"))
        {
            fri_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_10"))
        {
            sat_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_10"))
        {
            sun_10.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_11=(TextView)view.findViewById(R.id.mon_11);
        tue_11=(TextView)view.findViewById(R.id.tue_11);
        wed_11=(TextView)view.findViewById(R.id.wed_11);
        Th1_11=(TextView)view.findViewById(R.id.Th1_11);
        fri_11=(TextView)view.findViewById(R.id.fri_11);
        sat_11=(TextView)view.findViewById(R.id.sat_11);
        sun_11=(TextView)view.findViewById(R.id.sun_11);

        if(monday_list.contains("mon_11"))
        {
            mon_11.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_11"))
        {
            tue_11.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_11"))
        {
            wed_11.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_11"))
        {
            Th1_11.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_11"))
        {
            fri_11.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_11"))
        {
            sat_11.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_11"))
        {
            sun_11.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_12=(TextView)view.findViewById(R.id.mon_12);
        tue_12=(TextView)view.findViewById(R.id.tue_12);
        wed_12=(TextView)view.findViewById(R.id.wed_12);
        Th1_12=(TextView)view.findViewById(R.id.Th1_12);
        fri_12=(TextView)view.findViewById(R.id.fri_12);
        sat_12=(TextView)view.findViewById(R.id.sat_12);
        sun_12=(TextView)view.findViewById(R.id.sun_12);

        if(monday_list.contains("mon_12"))
        {
            mon_12.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_12"))
        {
            tue_12.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_12"))
        {
            wed_12.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_12"))
        {
            Th1_12.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_12"))
        {
            fri_12.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_12"))
        {
            sat_12.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_12"))
        {
            sun_12.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_13=(TextView)view.findViewById(R.id.mon_13);
        tue_13=(TextView)view.findViewById(R.id.tue_13);
        wed_13=(TextView)view.findViewById(R.id.wed_13);
        Th1_13=(TextView)view.findViewById(R.id.Th1_13);
        fri_13=(TextView)view.findViewById(R.id.fri_13);
        sat_13=(TextView)view.findViewById(R.id.sat_13);
        sun_13=(TextView)view.findViewById(R.id.sun_13);

        if(monday_list.contains("mon_13"))
        {
            mon_13.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_13"))
        {
            tue_13.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_13"))
        {
            wed_13.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_13"))
        {
            Th1_13.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_13"))
        {
            fri_13.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_13"))
        {
            sat_13.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_13"))
        {
            sun_13.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_14=(TextView)view.findViewById(R.id.mon_14);
        tue_14=(TextView)view.findViewById(R.id.tue_14);
        wed_14=(TextView)view.findViewById(R.id.wed_14);
        Th1_14=(TextView)view.findViewById(R.id.Th1_14);
        fri_14=(TextView)view.findViewById(R.id.fri_14);
        sat_14=(TextView)view.findViewById(R.id.sat_14);
        sun_14=(TextView)view.findViewById(R.id.sun_14);

        if(monday_list.contains("mon_14"))
        {
            mon_14.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_14"))
        {
            tue_14.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_14"))
        {
            wed_14.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_14"))
        {
            Th1_14.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_14"))
        {
            fri_14.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_14"))
        {
            sat_14.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_14"))
        {
            sun_10.setBackgroundResource(R.drawable.filled_box);
        }

        //

        mon_15=(TextView)view.findViewById(R.id.mon_15);
        tue_15=(TextView)view.findViewById(R.id.tue_15);
        wed_15=(TextView)view.findViewById(R.id.wed_15);
        Th1_15=(TextView)view.findViewById(R.id.Th1_15);
        fri_15=(TextView)view.findViewById(R.id.fri_15);
        sat_15=(TextView)view.findViewById(R.id.sat_15);
        sun_15=(TextView)view.findViewById(R.id.sun_15);

        if(monday_list.contains("mon_15"))
        {
            mon_15.setBackgroundResource(R.drawable.filled_box);
        }
        if(tuesday_list.contains("tue_15"))
        {
            tue_15.setBackgroundResource(R.drawable.filled_box);
        }
        if(wednesday_list.contains("wed_15"))
        {
            wed_15.setBackgroundResource(R.drawable.filled_box);
        }
        if(thursday_list.contains("Th1_15"))
        {
            Th1_10.setBackgroundResource(R.drawable.filled_box);
        }
        if(friday_list.contains("fri_15"))
        {
            fri_15.setBackgroundResource(R.drawable.filled_box);
        }
        if(saturday_list.contains("sat_15"))
        {
            sat_15.setBackgroundResource(R.drawable.filled_box);
        }
        if(sunday_list.contains("sun_15"))
        {
            sun_15.setBackgroundResource(R.drawable.filled_box);
        }

    }





}
