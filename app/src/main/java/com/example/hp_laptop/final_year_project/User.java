package com.example.hp_laptop.final_year_project;

import android.graphics.Bitmap;

public class User
{
    private String user_name;
    private String users_phone_number;
    private String users_email_id;
    private String users_image;
    private String doctor_or_patient;
    private String City;
    private String Address;
    private String clinic_name;
    private String specialization;
    private String cnic;
    private String pmdc;
    private String gender;
    private String provider;
    private String profile_photo;
    private String latitude;
    private String longitutude;
    private String experience;
    private String clnic_number;
    private String app_id;

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public  User()
    {

    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUsers_phone_number() {
        return users_phone_number;
    }

    public void setUsers_phone_number(String users_phone_number) {
        this.users_phone_number = users_phone_number;
    }

    public String getUsers_email_id() {
        return users_email_id;
    }

    public void setUsers_email_id(String users_email_id) {
        this.users_email_id = users_email_id;
    }

    public String getUsers_image() {
        return users_image;
    }

    public void setUsers_image(String users_image) {
        this.users_image = users_image;
    }

    public String getDoctor_or_patient() {
        return doctor_or_patient;
    }

    public void setDoctor_or_patient(String doctor_or_patient) {
        this.doctor_or_patient = doctor_or_patient;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitutude() {
        return longitutude;
    }

    public void setLongitutude(String longitutude) {
        this.longitutude = longitutude;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getClnic_number() {
        return clnic_number;
    }

    public void setClnic_number(String clnic_number) {
        this.clnic_number = clnic_number;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getCnic() {
        return cnic;
    }

    public void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public String getPmdc() {
        return pmdc;
    }

    public void setPmdc(String pmdc) {
        this.pmdc = pmdc;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }
}
