package com.example.hp_laptop.final_year_project;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class pateints_options extends AppCompatActivity {

    private ActionBar toolbar;
    BottomNavigationView navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pateints_options);

        toolbar = getSupportActionBar();

        navigation = (BottomNavigationView) findViewById(R.id.navigationView);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.info:
//                    toolbar.setTitle("Doctor Personal Information ");
                    Pateints_prescritions nextFrag= new Pateints_prescritions();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, nextFrag,"findThisFragment")
                            .addToBackStack(null)
                            .commit();
                    return true;
                case R.id.time:

                    Pateint_notifications_fragment Appointment_fragment = new Pateint_notifications_fragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.placeholder, Appointment_fragment,"findThisFragment")
                            .addToBackStack(null)
                            .commit();


                    return true;
                case R.id.location:
                    //   toolbar.setTitle("Location");
                    return true;
                case R.id.appoints:


                    return true;
            }
            return false;
        }
    };

}
