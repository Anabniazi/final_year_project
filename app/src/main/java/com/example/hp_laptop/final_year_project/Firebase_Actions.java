package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.content.Intent;
import android.icu.text.IDNA;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.example.hp_laptop.final_year_project.Dailogs.DismissLoading;
import static com.example.hp_laptop.final_year_project.Dailogs.Loading;

public class Firebase_Actions
{
    Context context;
    Context app_context;
    public void register_user(final Context application_context, final Context context, final  String email, final String password )
    {
        this.context =  context;
        this.app_context = application_context;


        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                // progressBar.setVisibility(View.GONE);
                if (task.isSuccessful()) {

                    Toast.makeText(application_context,"Your are now rigestered user",Toast.LENGTH_LONG).show();
                    String apid = FirebaseInstanceId.getInstance().getToken();
                    AppId phoneid = new AppId(apid);
                    DatabaseReference root = FirebaseDatabase.getInstance().getReference("blood_share");
                    root.child("phoneid").child((email.replace(".","")).replace("@","")).setValue(phoneid);


                } else {

                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(application_context, "You are already registered", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(application_context, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });

    }

    public void save_user(User user)
    {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("online_doctor");
        database.child("Users").child((Information.current_loged_in_user_email.replace(".","")).replace("@","")).setValue(user);
        Information.loged_in_user=user;
    }

    public void save_schedule(Schedule schedule)
    {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("online_doctor");
        database.child("schedules").child((Information.current_loged_in_user_email.replace(".","")).replace("@","")).setValue(schedule);
    }




    public void save_image(final Context context,String current_loged_in_user_email)
    {
        Log.d("Information",Information.current_loged_in_user_email+" null");
        this.context=context;
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        final StorageReference riversRef = storageRef.child("Usersprofilephotos").child((current_loged_in_user_email.replace(".","")).replace("@",""));

        UploadTask uploadTask = riversRef.putFile(Information.selected_image_Uri);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful())
                {
                    DismissLoading(context);
                    throw task.getException();
                }


                return riversRef.getDownloadUrl();
            }

        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    DismissLoading(context);
                    Information.selected_image_Url= task.getResult()+"";
                    Log.d("downloadurl_1",task.getResult()+"");
                   // save_user(make_object_of_user());
                } else {
                    // Handle failures
                    // ...
                }
            }
        });

        Loading(context);


    }

    public User make_object_of_user()
    {


        com.example.hp_laptop.final_year_project.User user = new com.example.hp_laptop.final_year_project.User();

        Log.d("Information.name",Information.user_name+" !null");
        user.setUser_name(Information.user_name);

        Log.d("Information.email",Information.users_email_id+" !null");
        user.setUsers_email_id(Information.users_email_id);

        Log.d("Information.users_phone",Information.users_phone_number+" !null");
        user.setUsers_phone_number(Information.users_phone_number);

        Log.d("Information.cnic",Information.cnic+" !null");
        user.setCnic(Information.cnic);

        Log.d("Information.gender",Information.gender+" !null");
        user.setGender(Information.gender);

        Log.d("Information.Profession",Information.users_Profession+" !null");
        user.setDoctor_or_patient(Information.users_Profession);

        Log.d("Information.pmdc",Information.pmdc+" !null");
        user.setPmdc(Information.pmdc);

        Log.d("Information.clinic_name",Information.clinic_name+" !null");
        user.setClinic_name(Information.clinic_name);

        Log.d("Information.specializ",Information.specialization+" !null");
        user.setSpecialization(Information.specialization);

        Log.d("Information.experience",Information.experience+" !null");
        user.setExperience(Information.experience);

        Log.d("Information.clinic_name",Information.clinic_name+" !null");
        user.setClnic_number(Information.clinic_name);

        Log.d("Information._image",Information.selected_image_Url+" !null");
        user.setProfile_photo(Information.selected_image_Url);

        Log.d("Information.city",Information.city+" !null");
        user.setCity(Information.city);

        Log.d("Information.latitude",Information.latitude+" !null");
        user.setLatitude(Information.latitude);

        Log.d("Information.longitutde",Information.longitutude+" !null");
        user.setLongitutude(Information.longitutude);

        user.setApp_id(FirebaseInstanceId.getInstance().getToken());



        return user;
    }

    public void check_if_this_user_already_exists(final Context context)
    {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("online_doctor").child("Users").child((Information.current_loged_in_user_email.replace(".","")).replace("@",""));

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                DismissLoading(context);  // stop loadingf
                User user = dataSnapshot.getValue(User.class);

                if(user==null)
                {
                    // save_image(context);
                    Intent intent = new Intent(context,Doctor_Profile.class);
                    context.startActivity(intent);
                }
                else
                {
                    Information.loged_in_user=user;

                    if(user.getDoctor_or_patient().equals("doctor"))
                    {
                        Intent intent = new Intent(context,Doctor_HomeScreen.class);
                        context.startActivity(intent);
                    }
                    else
                    {
                        Intent intent = new Intent(context,Home_Activity.class);
                        context.startActivity(intent);
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        /*ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                DismissLoading(context);  // stop loadingf
                User user = dataSnapshot.getValue(User.class);


               if(user==null)
               {
                  // save_image(context);
                   Intent intent = new Intent(context,Doctor_Profile.class);
                   context.startActivity(intent);
               }
               else
               {
                   Intent intent = new Intent(context,Doctor_HomeScreen.class);
                   context.startActivity(intent);
               }

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

                Log.d("hhhhh"," nnn");

            }
        });*/

        Loading(context); // start loadind
    }


    public String Geocoding(Context context,String Latitude,String Longitutde)
    {
        Geocoder gcd = new Geocoder(context, Locale.getDefault());


        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(Latitude), Double.parseDouble(Longitutde), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {


            Information.city=addresses.get(0).getLocality();
            return addresses.get(0).getLocality();
        }
        else {
            // do your stuff
        }

        return  "";
    }
}
