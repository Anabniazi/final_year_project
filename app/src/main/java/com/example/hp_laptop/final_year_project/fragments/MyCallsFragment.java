package com.example.hp_laptop.final_year_project.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hp_laptop.final_year_project.R;
import com.example.hp_laptop.final_year_project.adapters.LogCallAdapter;
import com.example.hp_laptop.final_year_project.interfaces.HomeIneractor;
import com.example.hp_laptop.final_year_project.models.Contact;
import com.example.hp_laptop.final_year_project.models.LogCall;
import com.example.hp_laptop.final_year_project.models.User;
import com.example.hp_laptop.final_year_project.utils.Helper;
import com.example.hp_laptop.final_year_project.views.MyRecyclerView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public class MyCallsFragment extends Fragment {
    private MyRecyclerView recyclerView;
    private LogCallAdapter chatAdapter;

    private Realm rChatDb;
    private User userMe;
    private RealmResults<LogCall> resultList;
    private ArrayList<LogCall> logCallDataList = new ArrayList<>();

    private RealmChangeListener<RealmResults<LogCall>> chatListChangeListener = new RealmChangeListener<RealmResults<LogCall>>() {
        @Override
        public void onChange(RealmResults<LogCall> element) {
            if (element != null && element.isValid() && element.size() > 0) {
                logCallDataList.clear();
                logCallDataList.addAll(rChatDb.copyFromRealm(element));
                setUserNamesAsInPhone();
            }
        }
    };
    private HomeIneractor homeInteractor;
    private Helper helper;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            homeInteractor = (HomeIneractor) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement HomeIneractor");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        homeInteractor = null;
        if (resultList != null)
            resultList.removeChangeListener(chatListChangeListener);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helper = new Helper(getContext());
        userMe = homeInteractor.getUserMe();
        Realm.init(getContext());
        rChatDb = Helper.getRealmInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_recycler, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);

        recyclerView.setEmptyView(view.findViewById(R.id.emptyView));
        recyclerView.setEmptyImageView(((ImageView) view.findViewById(R.id.emptyImage)));
        recyclerView.setEmptyTextView(((TextView) view.findViewById(R.id.emptyText)));
        recyclerView.setEmptyImage(R.drawable.ic_call_green_24dp);
        recyclerView.setEmptyText(getString(R.string.empty_log_call_list));

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RealmQuery<LogCall> query = rChatDb.where(LogCall.class).equalTo("myId", userMe.getId());//Query from chats whose owner is logged in user
        resultList = query.isNotNull("user").sort("timeUpdated", Sort.DESCENDING).findAll();//ignore forward list of messages and get rest sorted according to time

        logCallDataList.clear();
        logCallDataList.addAll(rChatDb.copyFromRealm(resultList));
        chatAdapter = new LogCallAdapter(getActivity(), logCallDataList);
        recyclerView.setAdapter(chatAdapter);

        resultList.addChangeListener(chatListChangeListener);
    }

    public void setUserNamesAsInPhone() {
        if (homeInteractor != null && logCallDataList != null) {
            for (LogCall logCall : logCallDataList) {
                User user = logCall.getUser();
                if (user != null) {
                    if (helper.getCacheMyUsers() != null && helper.getCacheMyUsers().containsKey(user.getId())) {
                        user.setNameInPhone(helper.getCacheMyUsers().get(user.getId()).getNameToDisplay());
                    } else {
                        for (Contact savedContact : homeInteractor.getLocalContacts()) {
                            if (Helper.contactMatches(user.getId(), savedContact.getPhoneNumber())) {
                                if (user.getNameInPhone() == null || !user.getNameInPhone().equals(savedContact.getName())) {
                                    user.setNameInPhone(savedContact.getName());
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (chatAdapter != null)
            chatAdapter.notifyDataSetChanged();
    }
}
