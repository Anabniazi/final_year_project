package com.example.hp_laptop.final_year_project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Home_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_);


        android.support.v4.app.FragmentManager fragmentManager=getSupportFragmentManager();
        Home_fragment googleFragment = new Home_fragment();
        fragmentManager.beginTransaction().addToBackStack(null).replace(R.id.placeholder,googleFragment).commit();
    }
}
