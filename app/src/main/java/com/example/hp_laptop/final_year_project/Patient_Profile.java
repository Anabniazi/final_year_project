package com.example.hp_laptop.final_year_project;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.scalified.fab.ActionButton;
import com.squareup.picasso.Picasso;

public class Patient_Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient__profile);
        de.hdodenhof.circleimageview.CircleImageView profileimage=(de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_profile_pic);
        final EditText t1=(EditText)findViewById(R.id.user_name);
        final EditText t2=(EditText)findViewById(R.id.user_email);
        final EditText t3=(EditText)findViewById(R.id.user_phone_number);
        final EditText t4=(EditText)findViewById(R.id.user_cnic);


          t1.setText(Information.user_name);
          t2.setText(Information.users_email_id);
          t3.setText(Information.doctor_or_patient);
        Picasso.with(Patient_Profile.this).load(Information.users_image).placeholder(R.drawable.de_profile_image).into(profileimage);

        ActionButton ab = (ActionButton)findViewById(R.id.select_location);
        ab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



        Button b1=(Button) findViewById(R.id.Ok_Button);
        Button b2=(Button) findViewById(R.id.Cancell_Button);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AuthUI.getInstance()
                        .signOut(Patient_Profile.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {


                                Intent i = new Intent(Patient_Profile.this,MainActivity.class);
                                startActivity(i);

                            }
                        });

                RadioGroup radiogroup= (RadioGroup)findViewById(R.id.genderp);
                radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup radioGroup, int i) {
                        switch (i){

                            case R.id.malep:
                                Information.gender="male";
                                break;
                            case  R.id.femalep:
                                Information.gender="female";
                                break;
                        }
                    }
                });


            }
        });
    }


}
