package com.example.hp_laptop.final_year_project;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class Patients_option_for_doctor extends AppCompatActivity {

    private ActionBar toolbar;
    BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_option_for_doctor);

        toolbar = getSupportActionBar();

        Detail_info_fragment_doctor nextFrag= new Detail_info_fragment_doctor();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, nextFrag,"findThisFragment")
                .addToBackStack(null)
                .commit();


        navigation = (BottomNavigationView) findViewById(R.id.navigationView);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.info:
//                    toolbar.setTitle("Doctor Personal Information ");
                    Detail_info_fragment_doctor nextFrag= new Detail_info_fragment_doctor();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, nextFrag,"findThisFragment")
                            .addToBackStack(null)
                            .commit();
                    return true;
                case R.id.time:

                    week_table week_table= new week_table();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, week_table,"findThisFragment")
                            .addToBackStack(null)
                            .commit();
                    return true;
                case R.id.location:
                 //   toolbar.setTitle("Location");
                    return true;
                case R.id.appoints:

                    Appointment_fragment Appointment_fragment = new Appointment_fragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, Appointment_fragment,"findThisFragment")
                            .addToBackStack(null)
                            .commit();
                    return true;
            }
            return false;
        }
    };



}
