package com.example.hp_laptop.final_year_project.interfaces;

/**
 * Created by a_man on 31-12-2017.
 */

public interface UserGroupSelectionDismissListener {
    void onUserGroupSelectDialogDismiss();

    void selectionDismissed();
}
