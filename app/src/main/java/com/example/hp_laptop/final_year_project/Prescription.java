package com.example.hp_laptop.final_year_project;

import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Prescription
{
    ArrayList<Medicine> medi_list;
    String date;
    String doctor_name;
    String doctor_pic;
    String total_days;

    public String getTotal_days() {
        return total_days;
    }

    public void setTotal_days(String total_days) {
        this.total_days = total_days;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getDoctor_pic() {
        return doctor_pic;
    }

    public void setDoctor_pic(String doctor_pic) {
        this.doctor_pic = doctor_pic;
    }

    public Prescription(ArrayList<Medicine> medi_list, String date)
    {
        this.medi_list=medi_list;
        this.date=date;
    }

    public Prescription()
    {

    }

    public ArrayList<Medicine> getMedi_list() {
        return medi_list;
    }

    public void setMedi_list(ArrayList<Medicine> medi_list) {
        this.medi_list = medi_list;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    /* String title_price;
    String title_time_label;
    String title_date_label;
    String disaese;
    String doctor_name;
    String clinic_name;
        *//*TextView title_status;
        TextView name;*//*


    // content views



   // ImageView content_avatar;
    String content_name_view;
    String medicine_1;
    String medicine_1_detail;
    String medicine_1_to_date;
    String medicine_2;
    String medicine_2_detail;
    String medicine_2_to_date;

    public Prescription()
    {

    }

    public Prescription(String title_price,String title_time_label,String title_date_label,String disaese, String doctor_name,String clinic_name,String content_name_view,String medicine_1,String medicine_1_detail, String medicine_1_to_date, String medicine_2,String medicine_2_detail, String medicine_2_to_date)
    {
        this.title_price=title_price;
        this.title_time_label=title_time_label;
        this.title_date_label=title_date_label;
        this.disaese=disaese;
        this.doctor_name=doctor_name;
        this.clinic_name=clinic_name;
        *//*TextView title_status;
        TextView name;*//*

        // ImageView content_avatar;
        this.content_name_view=content_name_view;
        this.medicine_1=medicine_1;
        this.medicine_1_detail=medicine_1_detail;
        this.medicine_1_to_date=medicine_1_to_date;
        this.medicine_2=medicine_2;
        this.medicine_2_detail=medicine_2_detail;
        this.medicine_2_to_date=medicine_2_to_date;
    }

    public String getTitle_price() {
        return title_price;
    }

    public void setTitle_price(String title_price) {
        this.title_price = title_price;
    }

    public String getTitle_time_label() {
        return title_time_label;
    }

    public void setTitle_time_label(String title_time_label) {
        this.title_time_label = title_time_label;
    }

    public String getTitle_date_label() {
        return title_date_label;
    }

    public void setTitle_date_label(String title_date_label) {
        this.title_date_label = title_date_label;
    }

    public String getDisaese() {
        return disaese;
    }

    public void setDisaese(String disaese) {
        this.disaese = disaese;
    }

    public String getDoctor_name() {
        return doctor_name;
    }

    public void setDoctor_name(String doctor_name) {
        this.doctor_name = doctor_name;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getContent_name_view() {
        return content_name_view;
    }

    public void setContent_name_view(String content_name_view) {
        this.content_name_view = content_name_view;
    }

    public String getMedicine_1() {
        return medicine_1;
    }

    public void setMedicine_1(String medicine_1) {
        this.medicine_1 = medicine_1;
    }

    public String getMedicine_1_detail() {
        return medicine_1_detail;
    }

    public void setMedicine_1_detail(String medicine_1_detail) {
        this.medicine_1_detail = medicine_1_detail;
    }

    public String getMedicine_1_to_date() {
        return medicine_1_to_date;
    }

    public void setMedicine_1_to_date(String medicine_1_to_date) {
        this.medicine_1_to_date = medicine_1_to_date;
    }

    public String getMedicine_2() {
        return medicine_2;
    }

    public void setMedicine_2(String medicine_2) {
        this.medicine_2 = medicine_2;
    }

    public String getMedicine_2_detail() {
        return medicine_2_detail;
    }

    public void setMedicine_2_detail(String medicine_2_detail) {
        this.medicine_2_detail = medicine_2_detail;
    }

    public String getMedicine_2_to_date() {
        return medicine_2_to_date;
    }

    public void setMedicine_2_to_date(String medicine_2_to_date) {
        this.medicine_2_to_date = medicine_2_to_date;
    }*/
}
