package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ramotion.foldingcell.FoldingCell;
import com.squareup.picasso.Picasso;

import java.util.HashSet;
import java.util.List;

/**
 * Simple example of ListAdapter for using with Folding Cell
 * Adapter holds indexes of unfolded elements for correct work with default reusable views behavior
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class FoldingCellListAdapter extends ArrayAdapter<Notification> {

    private HashSet<Integer> unfoldedIndexes = new HashSet<>();
    private View.OnClickListener defaultRequestBtnClickListener;
    Context context;
    List<Notification> notifications_list;

    public FoldingCellListAdapter(Context context, List<Notification> objects) {
        super(context, 0, objects);
        this.context=context;
        notifications_list=objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // get item for selected view
        final Notification item = getItem(position);
        // if cell is exists - reuse it, if not - create the new one from resource
        FoldingCell cell = (FoldingCell) convertView;
        ViewHolder viewHolder;
        if (cell == null) {
            viewHolder = new ViewHolder();
            LayoutInflater vi = LayoutInflater.from(getContext());
            cell = (FoldingCell) vi.inflate(R.layout.cell, parent, false);
            // binding view parts to view holder




            viewHolder.move_to_patient_history=cell.findViewById(R.id.move_to_patient_history);

           // viewHolder.title_price = cell.findViewById(R.id.title_price);
           // viewHolder.time_limit=cell.findViewById(R.id.title_time_label);
            viewHolder.title_time_label = cell.findViewById(R.id.title_time_label);
            viewHolder.title_date_label = cell.findViewById(R.id.title_date_label);
            viewHolder.disaese = cell.findViewById(R.id.disaese);
            viewHolder.location = cell.findViewById(R.id.location);
            viewHolder.title_requests_count = cell.findViewById(R.id.title_requests_count);
            viewHolder.name = cell.findViewById(R.id.name);
            viewHolder.contentRequestBtn = cell.findViewById(R.id.content_request_btn);
            viewHolder.title_status = cell.findViewById(R.id.title_status);
            viewHolder.content_reject_btn=cell.findViewById(R.id.content_reject_btn);


            //after opening

            viewHolder.requests_count_mock=cell.findViewById(R.id.number_of_request);
            viewHolder.patient_condition=cell.findViewById(R.id.patient_condition);
            viewHolder.pateint_name=cell.findViewById(R.id.pateint_name);
            viewHolder.pateint_iamge=cell.findViewById(R.id.pateint_iamge);
            viewHolder.pateint_name_with_iamge=cell.findViewById(R.id.pateint_name_with_iamge);
            viewHolder.content_from_address_1=cell.findViewById(R.id.content_from_address_1);
            viewHolder.content_from_address_2=cell.findViewById(R.id.content_from_address_2);
            viewHolder.delivery_time_mock=cell.findViewById(R.id.content_delivery_time);
            viewHolder.content_delivery_date=cell.findViewById(R.id.content_delivery_date);
            viewHolder.time_limit=cell.findViewById(R.id.time_limit);






            cell.setTag(viewHolder);
        } else {
            // for existing cell set valid valid state(without animation)
            if (unfoldedIndexes.contains(position)) {
                cell.unfold(true);
            } else {
                cell.fold(true);
            }
            viewHolder = (ViewHolder) cell.getTag();
        }

        if (null == item)
            return cell;

     //   viewHolder.title_price.setText("1000");
    //    viewHolder.time_limit.setText(item.getTime());
        viewHolder.title_date_label.setText(item.getDate());
        viewHolder.title_time_label.setText(item.getTime());
        viewHolder.title_requests_count.setText("10");
        viewHolder.name.setText(item.getSendernmae());
        viewHolder.title_status.setText(item.getStatus());
        viewHolder.disaese.setText(item.getDesease());
        viewHolder.location.setText(item.getSender_city());

        viewHolder.requests_count_mock.setText("10");
        viewHolder.patient_condition.setText(item.getPateint_condition());
        viewHolder.pateint_name.setText(item.getSendernmae());
        viewHolder.pateint_name_with_iamge.setText(item.getSendernmae());
        viewHolder.content_from_address_1.setText(item.getCity());
        viewHolder.content_from_address_2.setText("Pakistan");
        viewHolder.delivery_time_mock.setText(item.getTime());
        viewHolder.content_delivery_date.setText(item.getDate());
        viewHolder.time_limit.setText("60 minutes");

        Picasso.with(context)
                .load(item.getSenderpicurl())
                .into(viewHolder.pateint_iamge);

        viewHolder.move_to_patient_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context,"Move to user history",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context,patient_history.class);
                context.startActivity(intent);
            }
        });
        viewHolder.content_reject_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(context,"Reject",Toast.LENGTH_LONG).show();

                return_notification(item,"rej");

            }
        });

        viewHolder.contentRequestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context,"Accept",Toast.LENGTH_LONG).show();
                return_notification(item,"acc");
            }
        });


        // set custom btn handler for list item from that item
       /* if (item.getRequestBtnClickListener() != null)
        {
            viewHolder.contentRequestBtn.setOnClickListener(item.getRequestBtnClickListener());
        } else {
            // (optionally) add "default" handler if no handler found in item
            viewHolder.contentRequestBtn.setOnClickListener(defaultRequestBtnClickListener);
        }*/

        return cell;
    }

    // simple methods for register cell state changes
    public void registerToggle(int position) {
        if (unfoldedIndexes.contains(position))
            registerFold(position);
        else
            registerUnfold(position);
    }

    public void registerFold(int position) {
        unfoldedIndexes.remove(position);
    }

    public void registerUnfold(int position) {
        unfoldedIndexes.add(position);
    }

    public View.OnClickListener getDefaultRequestBtnClickListener() {
        return defaultRequestBtnClickListener;
    }

    public void setDefaultRequestBtnClickListener(View.OnClickListener defaultRequestBtnClickListener) {
        this.defaultRequestBtnClickListener = defaultRequestBtnClickListener;
    }

    // View lookup cache
    private static class ViewHolder {
        /*TextView price;
        TextView contentRequestBtn;
        TextView pledgePrice;
        TextView fromAddress;
        TextView toAddress;
        TextView requestsCount;
        TextView date;
        TextView time;
        TextView status;*/

        TextView contentRequestBtn;

        TextView title_price;
        TextView title_time_label;
        TextView title_date_label;
        TextView disaese;
        TextView location;
        TextView title_requests_count;
        TextView title_status;
        TextView name;


        // content views

        ImageView move_to_patient_history;
        TextView requests_count_mock;
        TextView patient_condition;
        TextView pateint_name;
        ImageView pateint_iamge;
        TextView pateint_name_with_iamge;
        TextView content_from_address_1;
        TextView content_from_address_2;
        TextView delivery_time_mock;
        TextView content_delivery_date;
        TextView time_limit;

        TextView content_reject_btn;


    }

    public void return_notification(Notification notification,String message)
    {
        patient patient = new patient();

        if(message.equals("rej"))
        {
            notification.setStatus("rejected");
            notification.setMesssage("Respected user your appointment with doctor "+notification.recievername+" is rejected");
        }
        if (message.equals("acc"))
        {

            patient.setName(notification.getSendernmae());
            patient.setEmail(notification.getSender_email());
            patient.setCity(notification.getSender_city());
            patient.setAddress(notification.getSenderlatitude()+","+notification.getSenderlongitutde());
            patient.setDisese(notification.getDesease());
            patient.setImage(notification.getSenderpicurl());
            patient.setNumber(notification.getSendernumber());
            patient.setStatus(notification.getStatus());
            patient.setNumber(notification.getSendernumber());
            patient.setAppid(notification.getSender_appid());


            notification.setStatus("accepted");
            notification.setMesssage("Respected user your appointment with doctor "+notification.recievername+" is accepted");
        }


        String revese;
        revese=notification.getRecievername();
        notification.setRecievername(notification.getSendernmae());
        notification.setSendernmae(revese);

        revese=notification.getReciever_email();
        notification.setReciever_email(notification.getSender_email());
        notification.setSender_email(revese);

        revese=notification.getRecieverpicurl();
        notification.setRecieverpicurl(notification.getSenderpicurl());
        notification.setSenderpicurl(revese);

        revese=notification.getReciever_city();
        notification.setReciever_city(notification.getSender_city());
        notification.setSender_city(revese);

        revese=notification.getReciever_appid();
        notification.setReciever_appid(notification.getSender_appid());
        notification.setSender_appid(revese);

        revese=notification.getRecievername();
        notification.setRecievername(notification.getSendernmae());
        notification.setSendernmae(revese);

        revese=notification.getRecievernumber();
        notification.setRecievernumber(notification.getSendernumber());
        notification.setSendernumber(revese);


        DatabaseReference database = FirebaseDatabase.getInstance().getReference("online_doctor");
      //  database.child("appointments_of_this_week").child((notification.getReciever_email().replace(".","")).replace("@","")).setValue(new Appointments_of_doctor(appointent_selected_already));

        database.child("Notifications").child((notification.getReciever_email().replace(".","")).replace("@","")).child(notification.getRecievername()+notification.getTime()).setValue(notification);
        database.child("Notifications").child((notification.getSender_email().replace(".","")).replace("@","")).child(notification.getSendernmae()+notification.getTime()).setValue(notification);

        database.child("pateints").child((notification.getSender_email().replace(".","")).replace("@","")).push().setValue(patient);



    }
}
