package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.scalified.fab.ActionButton;
import com.squareup.picasso.Picasso;


public class Appointment_Dialogue extends DialogFragment
{

    Button ok;
    Button cancel;
    ActionButton camerabutton;
    Context c = getActivity();
    de.hdodenhof.circleimageview.CircleImageView profileImage;
    private static final int GALLARY_REQUEST=2;
    private static final int CAMERA_REQUEST=3;
    static Bitmap photo=null;
    String picurl;
    String name;
    String email;
    String number;
    EditText editTexttime;
    EditText editTextdate;
    AutoCompleteTextView disesse;
    AutoCompleteTextView condition;
    View view;
    appointment_interface appointment_interface;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_appointment__dialogue, container, false);


        // Set transparent background and no title
        if (getDialog() != null && getDialog().getWindow() != null)
        {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setCancelable(false);

        ok=(Button)view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                appointment_interface = (appointment_interface)getTargetFragment();
                Log.d("appointment_interface_1",appointment_interface+"  !null");
                appointment_interface.finalizing_appointmnet(editTextdate.getText().toString(),editTexttime.getText().toString(),disesse.getText().toString(),condition.getText().toString());
                dismiss();

            }
        });
        cancel = (Button)view.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        profileImage =(de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.profilePic);

        disesse=(AutoCompleteTextView)view.findViewById(R.id.disese);
        disesse.setThreshold(1);
        String[] countries = getResources().getStringArray(R.array.specilizations);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,countries);
        disesse.setAdapter(adapter);

        condition=(AutoCompleteTextView)view.findViewById(R.id.condition);
        condition.setThreshold(1);
        String[] conditions = getResources().getStringArray(R.array.condition);
        ArrayAdapter<String> adapters = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,conditions);
        condition.setAdapter(adapters);

        Picasso.with(getActivity())
                .load(Information.selected_doctor.getProfile_photo())
                .into(profileImage);

        editTexttime=(EditText)view.findViewById(R.id.time);

        editTextdate=(EditText)view.findViewById(R.id.date);
        editTexttime.setText(Appointment_fragment.time);
        editTextdate.setText(Appointment_fragment.date);


        return view;
    }

    public interface appointment_interface
    {
        void finalizing_appointmnet(String date,String time,String disese,String condition);
    }

}
