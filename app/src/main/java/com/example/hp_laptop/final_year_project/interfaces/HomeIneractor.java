package com.example.hp_laptop.final_year_project.interfaces;

import com.example.hp_laptop.final_year_project.models.Contact;
import com.example.hp_laptop.final_year_project.models.User;

import java.util.ArrayList;

/**
 * Created by a_man on 01-01-2018.
 */

public interface HomeIneractor {
    User getUserMe();

    ArrayList<Contact> getLocalContacts();

}
