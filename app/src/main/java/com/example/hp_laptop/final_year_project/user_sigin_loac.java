package com.example.hp_laptop.final_year_project;

import android.app.Activity;
//  import android.app.FragmentManager;  by me
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
//     import android.app.Fragment; by me
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
       /* import com.google.android.gms.location.places.AutocompletePrediction;
        import com.google.android.gms.location.places.Place;
        import com.google.android.gms.location.places.PlaceBuffer;
        import com.google.android.gms.location.places.Places;*/
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import android.Manifest;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
/* import com.google.android.gms.location.places.Places;*/
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.scalified.fab.ActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;

import static android.support.constraint.Constraints.TAG;


public class user_sigin_loac extends Fragment implements OnMapReadyCallback, LocationListener, GoogleApiClient.OnConnectionFailedListener {


    private MapFragment m;
    private GoogleMap mMap;
    FusedLocationProviderClient fusedLocationProviderClient;
    private int REQUEST_PERMISSIONS_REQUEST_CODE = 125;

    protected Location mLastLocation;
    //   private AutoCompleteTextView autoCompleteTextView;
    //   private PlaceAutocompleteAdapter autocompleteAdapter;


    private String mLatitiudelabel;
    private String mLongitudelabel;

    private GoogleApiClient mGoogleApiClient;
    private Geocoder geocoder;
    private String address;


    private ActionButton camerabutton;


    private static LatLngBounds Lat_Long_Bounds = new LatLngBounds(new LatLng(-40, -168), new LatLng(71, 136));
    private LatLng cur_Latlng;
    private static View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        if (v != null) {

            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null) {

                parent.removeView(v);
            }

        }

        try {

            v = inflater.inflate(R.layout.fragment_google, container, false);

        } catch (InflateException e) {

            /* map is already there, just return view as it is */
        }

        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(new user_sigin_location(), this)
                .build();

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.d("locomotion", "Place: " + place.getName());
                Log.d("locomotion_1", "Place: " + place.getAddress());

                Information.latitude=place.getLatLng().latitude+"";
                Information.longitutude=place.getLatLng().longitude+"";



                show_marker(place.getLatLng());


            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });


       /* autocompleteAdapter = new PlaceAutocompleteAdapter(getActivity(),Places.getGeoDataClient(getActivity(), null),Lat_Long_Bounds,null);
        autoCompleteTextView.setAdapter(autocompleteAdapter);*/

        camerabutton = (ActionButton) v.findViewById(R.id.locationselected);
        camerabutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new Firebase_Actions().save_user(new Firebase_Actions().make_object_of_user());

                if(Information.doctor_or_patient.equals("doctor"))
                {

                    Intent intent = new Intent(getActivity(),ScedulaeActivity.class);
                    startActivity(intent);
                }

                else
                {
                    Intent intent = new Intent(getActivity(),Home_Activity.class);
                    startActivity(intent);
                }




            }
        });


        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mLatitiudelabel = "Latitude";
        mLongitudelabel = "Longitude";
        //FragmentManager fragmentManager = getActivity().getFragmentManager();
        // Log.i("mapo",fragmentManager+"           i");

        final SupportMapFragment myMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googlemap);


        // final MapFragment myMapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.googlemap);
        myMapFragment.getMapAsync(this);


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        startLocationPermissionRequest();

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        // sedinglocationfromfragment=(sendiddatafrommaptosellingitemdetail)context;
    }

    @Override
    public void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
        if (!checkPermissions()) {
            Log.e("premit", "nahi ha");
            //      requestPermissions();
        } else {
            Log.i("premit", " ha");
            //   getLastLocation();
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i("displaying permission", "Displaying permission rationale to provide additional context.");

        } else {
            Log.i("requesting permission", "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    private void startLocationPermissionRequest() {
        requestPermissions(
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i("onRequestPerm", "onRequestPermissionResult");

        if (grantResults.length <= 0) {
            // If user interaction was interrupted, the permission request is cancelled and you
            // receive empty arrays.
            Log.i("onRequestPerm", "User interaction was cancelled.");
        } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Permission granted.
            getlastlocation();
        } else {
            // Permission denied.

            // Notify the user via a SnackBar that they have rejected a core permission for the
            // app, which makes the Activity useless. In a real app, core permissions would
            // typically be best requested during a welcome-screen flow.

            // Additionally, it is important to remember that a permission might have been
            // rejected without asking the user for permission (device policy or "Never ask
            // again" prompts). Therefore, a user interface affordance is typically implemented
            // when permissions are denied. Otherwise, your app could appear unresponsive to
            // touches or interactions which have required permissions.
                /*showSnackbar(R.string.textwarn, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });*/
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void hidesoftkeyboard() {
        Log.i("hide", "hide");

        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public int hashCode() {
            return super.hashCode();
        }

        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.i("places status", places.getStatus().toString());
                places.release();
                return;
            } else {
                Log.i("placesstatus", places.getStatus().toString());
                Place place = places.get(0);

                try {
                    //   mPlace = new PlaceInfo();
                    //   mPlace.setName(place.getName().toString());
                    Log.d("lastinfo", "onResult: name: " + place.getName());
                    //   mPlace.setAddress(place.getAddress().toString());
                    Log.d("lastinfo", "onResult: address: " + place.getAddress());
//                mPlace.setAttributions(place.getAttributions().toString());
//                Log.d(TAG, "onResult: attributions: " + place.getAttributions());
                    //   mPlace.setId(place.getId());
                    Log.d("lastinfo", "onResult: id:" + place.getId());
                    //   mPlace.setLatlng(place.getLatLng());
                    Log.d("lastinfo", "onResult: latlng: " + place.getLatLng());
                    //   mPlace.setRating(place.getRating());
                    Log.d("lastinfo", "onResult: rating: " + place.getRating());
                    //   mPlace.setPhoneNumber(place.getPhoneNumber().toString());
                    Log.d("lastinfo", "onResult: phone number: " + place.getPhoneNumber());
                    //  mPlace.setWebsiteUri(place.getWebsiteUri());
                    Log.d("lastinfo", "onResult: website uri: " + place.getWebsiteUri());

                    //  Log.d("lastinfo", "onResult: place: " + mPlace.toString());
                } catch (NullPointerException e) {
                    Log.e("lastinfo", "onResult: NullPointerException: " + e.getMessage());
                }

                mMap.clear();
                cur_Latlng = new LatLng(place.getViewport().getCenter().latitude,
                        place.getViewport().getCenter().longitude); // giving your marker to zoom to your location area.
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(place.getViewport().getCenter().latitude,
                                place.getViewport().getCenter().longitude))
                        .title(place.getName() + ""));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(cur_Latlng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(9));
                places.release();

                places.release();
            }


        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(((AppCompatActivity) getActivity()).getSupportActionBar()!=null)
        {
            ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        }
        mGoogleApiClient.connect();
        if (!checkPermissions()) {
            Log.e("premit", "nahi ha");
            requestPermissions();
        } else {
            Log.i("premit", " ha");
            //getLastLocation();
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if((((AppCompatActivity) getActivity()).getSupportActionBar())!=null)
        {
            ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        }


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    public void getlastlocation()

    {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION}, 123);
            return;
        }
        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {

                        // Got last known location. In some rare situations this can be null.

                        if (location != null)
                        {

                            Toast.makeText(getActivity(),location.getLatitude()+"",Toast.LENGTH_LONG).show();

                            show_marker(new LatLng(location.getLatitude(),location.getLongitude()));

                        }
                    }
                });



    }


    public void show_marker(LatLng latlang)
    {
        if(latlang!=null && mMap!=null)
        {
           // Information.city=new Firebase_Actions().Geocoding(getActivity(),latlang.latitude+"",latlang.longitude+"");
            // Creating a marker


            Information.latitude=latlang.latitude+"";
            Information.longitutude=latlang.longitude+"";

            MarkerOptions markerOptions = new MarkerOptions();

            // Setting the position for the marker
            markerOptions.position(latlang);

            // Setting the title for the marker.
            // This will be displayed on taping the marker
            markerOptions.title(latlang.latitude + " : " + latlang.longitude);

            // Clears the previously touched position
            mMap.clear();

            // Animating to the touched position
            // googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlang, 12.0f));

            // Placing a marker on the touched position
            mMap.addMarker(markerOptions);
        }
    }


}

