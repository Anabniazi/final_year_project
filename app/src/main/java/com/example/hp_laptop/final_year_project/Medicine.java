package com.example.hp_laptop.final_year_project;

public class Medicine
{
    String date;
    String time;
    String limit;
    boolean mon;
    boolean tue;
    boolean wed;

    public Medicine()
    {

    }

    public Medicine(String date,String time,String limit,boolean mon,boolean tue,boolean wed)
    {
        this.date=date;
        this.time=time;
        this.limit=limit;
        this.mon=mon;
        this.tue=tue;
        this.wed=wed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public boolean isMon() {
        return mon;
    }

    public void setMon(boolean mon) {
        this.mon = mon;
    }

    public boolean isTue() {
        return tue;
    }

    public void setTue(boolean tue) {
        this.tue = tue;
    }

    public boolean isWed() {
        return wed;
    }

    public void setWed(boolean wed) {
        this.wed = wed;
    }
}
