package com.example.hp_laptop.final_year_project;

public class Notification
{
    String sendernmae;
    String recievername;
    String sendernumber;
    String recievernumber;
    String senderlatitude;
    String senderlongitutde;
    String recieverlatitude;
    String recieverlongitutde;
    String messsage;
    String senderpicurl;
    String recieverpicurl;
    String dtate;
    String time;
    String bloodgroup;
    String reciever_appid;
    String noti_reciever_email;
    String status;
    String type;
    String city;
    String reciever_email;
    String sender_email;
    String sender_appid;
    String date;
    String desease;
    String sender_city;
    String reciever_city;
    String pateint_condition;
    String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPateint_condition() {
        return pateint_condition;
    }

    public void setPateint_condition(String pateint_condition) {
        this.pateint_condition = pateint_condition;
    }

    public String getSender_city() {
        return sender_city;
    }

    public void setSender_city(String sender_city) {
        this.sender_city = sender_city;
    }

    public String getReciever_city() {
        return reciever_city;
    }

    public void setReciever_city(String reciever_city) {
        this.reciever_city = reciever_city;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSender_appid() {
        return sender_appid;
    }

    public void setSender_appid(String sender_appid) {
        this.sender_appid = sender_appid;
    }

    public String getReciever_email() {
        return reciever_email;
    }

    public void setReciever_email(String reciever_email) {
        this.reciever_email = reciever_email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoti_reciever_email() {
        return noti_reciever_email;
    }

    public void setNoti_reciever_email(String noti_reciever_email) {
        this.noti_reciever_email = noti_reciever_email;
    }

    public String getReciever_appid() {
        return reciever_appid;
    }

    public void setReciever_appid(String reciever_appid) {
        this.reciever_appid = reciever_appid;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getDtate() {
        return dtate;
    }

    public void setDtate(String dtate) {
        this.dtate = dtate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSender_email() {
        return sender_email;
    }

    public void setSender_email(String sender_email) {
        this.sender_email = sender_email;
    }

    public Notification(String sendernmae, String recievername, String sendernumber, String recievernumber, String senderlatitude, String senderlongitutde, String recieverlatitude, String recieverlongitutde, String senderpicurl, String date, String time, String bloodgroup, String message, String reciever_appid, String noti_reciever_email, String status, String city, String t, String reciever_email, String sender_email,String sender_appid)
    {
        this.sendernmae=sendernmae;
        this.recievername=recievername;
        this.sendernumber=sendernumber;
        this.recievernumber=recievernumber;
        this.senderlatitude=senderlatitude;
        this.senderlongitutde=senderlongitutde;
        this.recieverlatitude=recieverlatitude;
        this.recieverlongitutde=recieverlongitutde;
        this.senderpicurl=senderpicurl;
        this.bloodgroup=bloodgroup;
        this.dtate=date;
        this.time=time;
        this.messsage=message;
        this.reciever_appid=reciever_appid;
        this.noti_reciever_email=noti_reciever_email;
        this.status=status;
        this.city=city;
        this.reciever_email=reciever_email;
        this.sender_email=sender_email;
        this.sender_appid=sender_appid;

    }

    public Notification(String sendernmae, String recievername, String sendernumber, String recievernumber, String senderlatitude, String senderlongitutde, String recieverlatitude, String recieverlongitutde, String senderpicurl, String date, String time,String bloodgroup, String message,String reciever_appid,String noti_reciever_email,String status,String type)
    {
        this.sendernmae=sendernmae;
        this.recievername=recievername;
        this.sendernumber=sendernumber;
        this.recievernumber=recievernumber;
        this.senderlatitude=senderlatitude;
        this.senderlongitutde=senderlongitutde;
        this.recieverlatitude=recieverlatitude;
        this.recieverlongitutde=recieverlongitutde;
        this.senderpicurl=senderpicurl;
        this.bloodgroup=bloodgroup;
        this.dtate=date;
        this.time=time;
        this.messsage=message;
        this.reciever_appid=reciever_appid;
        this.noti_reciever_email=noti_reciever_email;
        this.status=status;
        this.type=type;

    }

    public Notification()
    {}

    public String getSendernmae() {
        return sendernmae;
    }

    public void setSendernmae(String sendernmae) {
        this.sendernmae = sendernmae;
    }

    public String getRecievername() {
        return recievername;
    }

    public void setRecievername(String recievername) {
        this.recievername = recievername;
    }

    public String getSendernumber() {
        return sendernumber;
    }

    public void setSendernumber(String sendernumber) {
        this.sendernumber = sendernumber;
    }

    public String getRecievernumber() {
        return recievernumber;
    }

    public void setRecievernumber(String recievernumber) {
        this.recievernumber = recievernumber;
    }

    public String getSenderlatitude() {
        return senderlatitude;
    }

    public void setSenderlatitude(String senderlatitude) {
        this.senderlatitude = senderlatitude;
    }

    public String getSenderlongitutde() {
        return senderlongitutde;
    }

    public void setSenderlongitutde(String senderlongitutde) {
        this.senderlongitutde = senderlongitutde;
    }

    public String getRecieverlatitude() {
        return recieverlatitude;
    }

    public void setRecieverlatitude(String recieverlatitude) {
        this.recieverlatitude = recieverlatitude;
    }

    public String getRecieverlongitutde() {
        return recieverlongitutde;
    }

    public void setRecieverlongitutde(String recieverlongitutde) {
        this.recieverlongitutde = recieverlongitutde;
    }

    public String getSenderpicurl() {
        return senderpicurl;
    }

    public void setSenderpicurl(String senderpicurl) {
        this.senderpicurl = senderpicurl;
    }


    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRecieverpicurl() {
        return recieverpicurl;
    }

    public void setRecieverpicurl(String recieverpicurl) {
        this.recieverpicurl = recieverpicurl;
    }

    public String getDesease() {
        return desease;
    }

    public void setDesease(String desease) {
        this.desease = desease;
    }
}
