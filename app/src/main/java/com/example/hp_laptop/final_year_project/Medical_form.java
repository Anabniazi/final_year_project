package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Medical_form extends AppCompatActivity {

    patient patient;
    ImageView content_avatar;
    TextView content_name_view;
    LinearLayout main_layout;
    com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton next;
    com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton add;
    ArrayList<ImageButton> delete_buttons =  new ArrayList<>();
    ArrayList<Medicine> medicines =  new ArrayList<>();
    TextView date;
    String today_date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
    TextView head_image_left_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_form);

        Intent intent = getIntent();
        patient = intent.getParcelableExtra("selected_pateint");

        main_layout=(LinearLayout)findViewById(R.id.main_layout);
        head_image_left_text=(EditText)findViewById(R.id.head_image_left_text);
      date=(TextView)findViewById(R.id.date);
      date.setText(today_date);
        next=(com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton)findViewById(R.id.next);
        add=(com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton)findViewById(R.id.add);


        Log.d("tttttttt",patient+"  !=null");

        date=(TextView)findViewById(R.id.date);
        content_avatar=(ImageView)findViewById(R.id.content_avatar);
        content_name_view=(TextView) findViewById(R.id.content_name_view);
        Picasso.with(this).load(patient.getImage()).placeholder(R.drawable.de_profile_image).into(content_avatar);
        content_name_view.setText(patient.getName());


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(Medical_form.this,"adding value to databse",Toast.LENGTH_LONG).show();
                Prescription press = new Prescription();
                press.setDoctor_name(Information.loged_in_user.getUser_name());
                press.setDoctor_pic(Information.loged_in_user.getProfile_photo());
                press.setDate(today_date);
                press.setTotal_days(head_image_left_text.getText().toString());
                press.setMedi_list(medicines);

                DatabaseReference database = FirebaseDatabase.getInstance().getReference("online_doctor");
                database.child("prescriptions").child((patient.getEmail().replace(".","")).replace("@","")).push().setValue(press);

                Notification notification =get_notification_object();
                database.child("Notifications").child((patient.getEmail().replace(".","")).replace("@","")).child(notification.getSendernmae()+notification.getTime()).setValue(notification);


                Toast.makeText(Medical_form.this, "Prescription is sent to "+patient.getName(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Medical_form.this,Doctor_Patients.class);
                startActivity(intent);

            }
        });


        /*final ImageButton pres_accp =(ImageButton)findViewById(R.id.pres_accp);
        pres_accp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                EditText content_delivery_time = (EditText) findViewById(R.id.content_delivery_time);
                EditText content_delivery_date =(EditText) findViewById(R.id.content_delivery_date);
                EditText time_limit =(EditText)findViewById(R.id.time_limit);
                CheckBox morn = (CheckBox) findViewById(R.id.morn);
                CheckBox lunch = (CheckBox) findViewById(R.id.lunch);
                CheckBox dinner = (CheckBox) findViewById(R.id.dinner);

                Log.d("checkoo",content_delivery_time.getText() +" !=null");

                if(( content_delivery_time.getText()==null || content_delivery_date.getText()==null || time_limit.getText()==null  ) || (morn.isChecked()==false && lunch.isChecked()==false && dinner.isChecked()==false))
                {
                    Toast.makeText(Medical_form.this,"kindly fill prescription",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(Medical_form.this,"Adding",Toast.LENGTH_LONG).show();
                    Medicine medicine = new Medicine(content_delivery_date.getText().toString(),content_delivery_time.getText().toString(),time_limit.getText().toString(),morn.isChecked(),lunch.isChecked(),dinner.isChecked());
                    medicines.add(medicine);
                    pres_accp.setVisibility(View.INVISIBLE);
                }
            }
        });*/




    }

    public void onAddField(View v)
    {
        if(medicines.size()== delete_buttons.size())
        {

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.field, null);
            main_layout.addView(rowView, main_layout.getChildCount() - 1);

        final     AutoCompleteTextView content_delivery_time = (AutoCompleteTextView) findViewById(R.id.content_delivery_time);
        final    ArrayAdapter<String> adapter = new ArrayAdapter<String> (Medical_form.this,android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.medicines));
            content_delivery_time.setAdapter(adapter);
            content_delivery_time.setThreshold(1);



        final     EditText content_delivery_date =(EditText) findViewById(R.id.content_delivery_date);
        final     EditText time_limit =(EditText)findViewById(R.id.time_limit);
        final     CheckBox morn = (CheckBox) findViewById(R.id.morn);
        final     CheckBox lunch = (CheckBox) findViewById(R.id.lunch);
        final     CheckBox dinner = (CheckBox) findViewById(R.id.dinner);

            delete_buttons.add((ImageButton) rowView.findViewById(R.id.pres_del));
            final ImageButton del =(ImageButton)rowView.findViewById(R.id.pres_del);
            final ImageButton pres_accp =(ImageButton)rowView.findViewById(R.id.pres_accp);
            pres_accp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {


                    if(( content_delivery_time.getText()==null || content_delivery_date.getText()==null || time_limit.getText()==null  ) || (morn.isChecked()==false && lunch.isChecked()==false && dinner.isChecked()==false))
                    {
                        Toast.makeText(Medical_form.this,"kindly fill prescription",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Medicine medicine = new Medicine(content_delivery_date.getText().toString(),content_delivery_time.getText().toString(),time_limit.getText().toString(),morn.isChecked(),lunch.isChecked(),dinner.isChecked());
                        medicines.add(medicine);
                        //delete_buttons.add((ImageButton) rowView.findViewById(R.id.pres_del));
                        pres_accp.setVisibility(View.INVISIBLE);
                        del.setVisibility(View.VISIBLE);
                    }
                }
            });

        }

        else
        {
            Toast.makeText(this,"Kindly first complete and add lastone",Toast.LENGTH_LONG).show();
        }
    }

    public void onDelete(View v)
    {

        Log.d("view_recieved",v+"");
        for(int i=0;i<delete_buttons.size();i++)
        {
            Log.d("view_looped",delete_buttons.get(i)+"");
            if(v==delete_buttons.get(i))
            {
                Toast.makeText(Medical_form.this,"Inside",Toast.LENGTH_LONG).show();
                delete_buttons.remove(i);
                medicines.remove(i);
                main_layout.removeView(((View)((View) v.getParent()).getParent()));

            }
        }
    }

    public Notification get_notification_object()
    {

        Notification notification = new Notification();
        notification.setDate(today_date);
        notification.setTime(getCurrentTime());

            notification.setSendernmae(Information.loged_in_user.getUser_name());
            notification.setRecievername(patient.getName());
            notification.setRecievernumber(patient.getNumber());
            notification.setSendernumber(Information.loged_in_user.getUsers_phone_number());
            notification.setSenderlatitude(Information.loged_in_user.getLatitude());
            notification.setRecieverlatitude(patient.getAddress());
            notification.setSenderlongitutde(patient.getAddress());
            notification.setRecieverlongitutde(patient.getAddress());
            notification.setMesssage("You hav e new Prescription from "+Information.loged_in_user.getUser_name());
            notification.setSender_email(Information.loged_in_user.getUsers_email_id());
            notification.setReciever_email(patient.getEmail());
            notification.setSenderpicurl(Information.loged_in_user.getProfile_photo());
            notification.setRecieverpicurl(patient.getImage());
            notification.setSender_appid(Information.loged_in_user.getApp_id());
            notification.setReciever_appid(Information.loged_in_user.getApp_id());
            //notification.setTime("1200");


        return notification;

    }

    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = mdformat.format(calendar.getTime())+"";

        return strDate;

    }
}
