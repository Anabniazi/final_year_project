package com.example.hp_laptop.final_year_project;

public class Doctors {



    String user_name;
    String users_phone_number;
    String users_email_id;
    String users_image;
    String Cnic;
    String Experience;
    String City;
    String Address;
    String Clinic_number;
    String clinic_name;
    String specialization;
    String pmdc;
    String gender;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUsers_phone_number() {
        return users_phone_number;
    }

    public String getUsers_email_id() {
        return users_email_id;
    }

    public String getUsers_image() {
        return users_image;
    }

    public String getCnic() {
        return Cnic;
    }

    public String getPmdc() {
        return pmdc;
    }

    public String getExperience() {
        return Experience;
    }

    public String getCity() {
        return City;
    }

    public String getAddress() {
        return Address;
    }

    public String getClinic_number() {
        return Clinic_number;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setUser_name(String user_name) {

        this.user_name = user_name;
    }

    public void setUsers_phone_number(String users_phone_number) {
        this.users_phone_number = users_phone_number;
    }

    public void setUsers_email_id(String users_email_id) {
        this.users_email_id = users_email_id;
    }

    public void setUsers_image(String users_image) {
        this.users_image = users_image;
    }

    public void setCnic(String cnic) {
        Cnic = cnic;
    }

    public void setPmdc(String pmdc) {
        this.pmdc = pmdc;
    }

    public void setExperience(String experience) {
        Experience = experience;
    }

    public void setCity(String city) {
        City = city;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setClinic_number(String clinic_number) {
        Clinic_number = clinic_number;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }


}
