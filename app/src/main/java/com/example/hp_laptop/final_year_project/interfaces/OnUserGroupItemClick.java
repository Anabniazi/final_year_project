package com.example.hp_laptop.final_year_project.interfaces;

import android.view.View;

import com.example.hp_laptop.final_year_project.models.Group;
import com.example.hp_laptop.final_year_project.models.User;

/**
 * Created by a_man on 5/10/2017.
 */

public interface OnUserGroupItemClick {
    void OnUserClick(User user, int position, View userImage);
    void OnGroupClick(Group group, int position, View userImage);
}
