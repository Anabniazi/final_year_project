package com.example.hp_laptop.final_year_project;

//import android.support.v4.app.DialogFragment;
import android.app.DialogFragment;
import android.content.Intent;
import android.drm.DrmStore;
import android.graphics.Bitmap;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.scalified.fab.ActionButton;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.zip.Inflater;

public class signin_user_information_display extends DialogFragment
{
    View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        view = inflater.inflate(R.layout.sigin_user_information,container,false);
       setCancelable(false);
        EditText t1=(EditText)view.findViewById(R.id.user_name);
        EditText t2=(EditText)view.findViewById(R.id.user_email);
        EditText t3=(EditText)view.findViewById(R.id.user_phone_number);
        de.hdodenhof.circleimageview.CircleImageView profileimage=(de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.user_profile_pic);
        RadioGroup radiogroup=(RadioGroup)view.findViewById(R.id.doctor_or_patient);
        Button ok_button=(Button)view.findViewById(R.id.Ok_Button);
        Button cancel_button=(Button)view.findViewById(R.id.Cancell_Button);

        t1.setText(Information.user_name);
        t2.setText(Information.users_email_id);
        t3.setText(Information.users_phone_number);

        Picasso.with(getActivity()).load(Information.users_image).placeholder(R.drawable.de_profile_image).into(profileimage);

        profileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
              Intent intent = new Intent();
              intent.setType("image/*");
              intent.setAction(Intent.ACTION_GET_CONTENT);
              startActivityForResult(Intent.createChooser(intent,"select files"),456);
            }
        });

       radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(RadioGroup radioGroup, int i) {
               switch(i)
               {
                   case R.id.doctor:
                       Information.doctor_or_patient="doctor";
                       break;
                   case R.id.patient:
                       Information.doctor_or_patient="patinet";
                       break;
               }

           }
       });

       cancel_button.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               AuthUI.getInstance()
                       .signOut(getActivity())
                       .addOnCompleteListener(new OnCompleteListener<Void>() {
                           public void onComplete(@NonNull Task<Void> task) {

                               dismiss();
                               Intent i = new Intent(getActivity(),MainActivity.class);
                               startActivity(i);

                           }
                       });
           }
       });



        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            Bitmap pic= MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),data.getData());
            de.hdodenhof.circleimageview.CircleImageView profileimage=(de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.user_profile_pic);
            profileimage.setImageBitmap(pic);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
