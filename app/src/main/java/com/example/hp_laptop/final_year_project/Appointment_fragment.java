package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.scalified.fab.ActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class Appointment_fragment extends Fragment implements Appointment_Dialogue.appointment_interface {

    View view;

    static  String time;
    static  String date;

    String monay_list;
    String tuesday_list;
    String wednesday_list;
    String thursday_list;
    String friday_list;
    String saturday_list;
    String sunday_list;

    TextView selectd;

    boolean all_selected = false;


    String appointments="";
    String appointent_selected_already="";



    TextView mon_1,tue_1,wed_1,Th1_1,fri_1,sat_1,sun_1;
    TextView mon_2,tue_2,wed_2,Th1_2,fri_2,sat_2,sun_2;
    TextView mon_4,tue_4,wed_4,Th1_4,fri_4,sat_4,sun_4;
    TextView mon_5,tue_5,wed_5,Th1_5,fri_5,sat_5,sun_5;
    TextView mon_7,tue_7,wed_7,Th1_7,fri_7,sat_7,sun_7;
    TextView mon_8,tue_8,wed_8,Th1_8,fri_8,sat_8,sun_8;
    TextView mon_9,tue_9,wed_9,Th1_9,fri_9,sat_9,sun_9;
    TextView mon_10,tue_10,wed_10,Th1_10,fri_10,sat_10,sun_10;
    TextView mon_11,tue_11,wed_11,Th1_11,fri_11,sat_11,sun_11;
    TextView mon_12,tue_12,wed_12,Th1_12,fri_12,sat_12,sun_12;
    TextView mon_13,tue_13,wed_13,Th1_13,fri_13,sat_13,sun_13;
    TextView mon_14,tue_14,wed_14,Th1_14,fri_14,sat_14,sun_14;
    TextView mon_15,tue_15,wed_15,Th1_15,fri_15,sat_15,sun_15;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_appointment_fragment, container, false);

        Log.d("doctrian_email_000000",Information.selected_doctor.getUsers_email_id()+ " !null");



        com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton next = (com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton)view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(),"button clciked",Toast.LENGTH_LONG).show();


                set_dae(appointments);
                set_time(appointments);

                FragmentManager fm = getActivity().getSupportFragmentManager();
                Appointment_Dialogue appointment_Dialogue = new Appointment_Dialogue();
                appointment_Dialogue.setTargetFragment(Appointment_fragment.this,0);
                appointment_Dialogue.show(fm, "fragment_edit_name");



            }
        });



        download_time_table();

        return view;
    }


    public void download_time_table()
    {

        // check it hre ?? bbjf bbua  bjha  cha  bvvau  buuw  bckkud   bwk

        Log.d("check_time_table","check_time_table_out");

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("schedules").child((Information.selected_doctor.getUsers_email_id().replace(".","")).replace("@",""));
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot

                        Log.d("check_time_table","check_time_table_in");
                        Schedule schedule = dataSnapshot.getValue(Schedule.class);

                        monay_list=schedule.getMonday();
                        tuesday_list=schedule.getTuesday();
                        wednesday_list=schedule.getWednesday();
                        thursday_list=schedule.getThursday();
                        friday_list=schedule.getFriday();
                        saturday_list=schedule.getSaturday();
                        sunday_list=schedule.getSunday();
                        //mark_days();


                        Log.d("monay_list",monay_list);
                        Log.d("tuesday_list",tuesday_list);
                        Log.d("wednesday_list",wednesday_list);
                        Log.d("thursday_list",thursday_list);
                        Log.d("saturday_list",saturday_list);
                        Log.d("sunday_list",sunday_list);

                        download_time_table_of_appointmesnts();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });

    }

    public void download_time_table_of_appointmesnts()
    {

        Log.d("check_time_table","check_time_table_out");

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("appointments_of_this_week").child((Information.selected_doctor.getUsers_email_id().replace(".","")).replace("@",""));
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot

                        Log.d("check_time_table","check_time_table_in");
                        Appointments_of_doctor   doctor_appointments = dataSnapshot.getValue(Appointments_of_doctor.class);

                        if(doctor_appointments!=null)
                        {
                            appointent_selected_already = doctor_appointments.getAppointent_selected_already();
                        }


                        if((appointent_selected_already==null) || (appointent_selected_already.equals("")))
                        {
                            appointent_selected_already="";
                        }

                        mark_days();

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });

    }

    public void mark_days()
    {
        mon_1=(TextView)view.findViewById(R.id.mon_1);
        mon_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mon_1.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_1";
             //   Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_1;

                }

                if(selectd==null)
                {
                    selectd=mon_1;

                }

            }
        });

        if(monay_list.contains("mon_1"))
        {

            Log.d("first_in","first_in");

            mon_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_1"))
            {
                Log.d("second_in","second_in");
                mon_1.setVisibility(View.INVISIBLE);
            }

        }
        else
        {
            mon_1.setVisibility(View.INVISIBLE);
        }

        tue_1=(TextView)view.findViewById(R.id.tue_1);
        tue_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_1.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_1";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_1;

                    Toast.makeText(getActivity(),"last un selected",Toast.LENGTH_LONG).show();

                }
            }
        });
        if(tuesday_list.contains("tue_1"))
        {
            tue_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_1"))
            {
                mon_1.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_1.setVisibility(View.INVISIBLE);
        }

        wed_1=(TextView)view.findViewById(R.id.wed_1);
        wed_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                appointments="";
                appointments=appointments+"wed_1";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                wed_1.setBackgroundResource(R.drawable.taken_box);

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_1;

                }
            }
        });
        if(wednesday_list.contains("wed_1"))
        {
            wed_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_1"))
            {
                wed_1.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_1.setVisibility(View.INVISIBLE);
        }


        Th1_1=(TextView)view.findViewById(R.id.Th1_1);
        Th1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_1.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_1";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_1;

                }
            }
        });
        if(thursday_list.contains("Th1_1"))
        {
            Th1_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_1"))
            {
                Th1_1.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_1.setVisibility(View.INVISIBLE);
        }

        fri_1=(TextView)view.findViewById(R.id.fri_1);
        fri_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_1.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_1";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_1;

                }
            }
        });
        if(friday_list.contains("fri_1"))
        {
            fri_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_1"))
            {
                fri_1.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_1.setVisibility(View.INVISIBLE);
        }

        sat_1=(TextView)view.findViewById(R.id.sat_1);
        sat_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_1.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_1";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_1;

                }
            }
        });
        if(saturday_list.contains("sat_1"))
        {
            sat_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_1"))
            {
                sat_1.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_1.setVisibility(View.INVISIBLE);
        }

        sun_1=(TextView)view.findViewById(R.id.sun_1);
        sun_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sun_1.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_1";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_1;

                }
            }
        });
        if(sunday_list.contains("sun_1"))
        {
            sun_1.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_1"))
            {
                sun_1.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_1.setVisibility(View.INVISIBLE);
        }

        // raow_2


        mon_2=(TextView)view.findViewById(R.id.mon_2);
        tue_2=(TextView)view.findViewById(R.id.tue_2);
        wed_2=(TextView)view.findViewById(R.id.wed_2);
        Th1_2=(TextView)view.findViewById(R.id.Th1_2);
        fri_2=(TextView)view.findViewById(R.id.fri_2);
        sat_2=(TextView)view.findViewById(R.id.sat_2);
        sun_2=(TextView)view.findViewById(R.id.sun_2);

        mon_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mon_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_2;

                }
            }
        });

        if(monay_list.contains("mon_2"))
        {
            mon_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_2"))
            {
                mon_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_2.setVisibility(View.INVISIBLE);
        }




        tue_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_2;

                }
            }
        });
        if(tuesday_list.contains("tue_2"))
        {
            tue_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_2"))
            {
                tue_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_2.setVisibility(View.INVISIBLE);
        }





        wed_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               wed_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_2;

                }
            }
        });
        if(wednesday_list.contains("wed_2"))
        {
            wed_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_2"))
            {
                wed_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_2.setVisibility(View.INVISIBLE);
        }






        Th1_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();


                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_2;

                }
            }
        });
        if(thursday_list.contains("Th1_2"))
        {
            Th1_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_2"))
            {
                Th1_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_2.setVisibility(View.INVISIBLE);
        }


        fri_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();


                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_2;

                }
            }
        });
        if(friday_list.contains("fri_2"))
        {
            fri_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_2"))
            {
                fri_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_2.setVisibility(View.INVISIBLE);
        }

        sat_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_2;

                }
             }
        });
        if(saturday_list.contains("sat_2"))
        {
            sat_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_2"))
            {
                fri_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_2.setVisibility(View.INVISIBLE);
        }


        sun_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sun_2.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_2";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_2;

                }
            }
        });
        if(sunday_list.contains("sun_2"))
        {
            sun_2.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_2"))
            {
                sun_2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_2.setVisibility(View.INVISIBLE);
        }

        // row 3

        mon_4=(TextView)view.findViewById(R.id.mon_4);
        tue_4=(TextView)view.findViewById(R.id.tue_4);
        wed_4=(TextView)view.findViewById(R.id.wed_4);
        Th1_4=(TextView)view.findViewById(R.id.Th1_4);
        fri_4=(TextView)view.findViewById(R.id.fri_4);
        sat_4=(TextView)view.findViewById(R.id.sat_4);
        sun_4=(TextView)view.findViewById(R.id.sun_4);

        if(monay_list.contains("mon_4"))
        {
            mon_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_4"))
            {
                mon_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_4.setVisibility(View.INVISIBLE);
        }
        mon_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mon_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_4;

                }
            }
        });

        if(tuesday_list.contains("tue_4"))
        {
            tue_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_4"))
            {
                tue_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_4.setVisibility(View.INVISIBLE);
        }
        tue_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tue_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_4;

                }
            }
        });
        if(wednesday_list.contains("wed_4"))
        {
            wed_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_4"))
            {
                wed_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_4.setVisibility(View.INVISIBLE);
        }
        wed_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_4;

                }
            }
        });
        if(thursday_list.contains("Th1_4"))
        {
            Th1_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_4"))
            {
                Th1_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_4.setVisibility(View.INVISIBLE);
        }
        Th1_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_4;

                }

            }
        });
        if(friday_list.contains("fri_4"))
        {
            fri_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_4"))
            {
                fri_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_4.setVisibility(View.INVISIBLE);
        }
        fri_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_4;

                }

            }
        });
        if(saturday_list.contains("sat_4"))
        {
            sat_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_4"))
            {
                sat_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_4.setVisibility(View.INVISIBLE);
        }
        sat_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_4;

                }

            }
        });
        if(sunday_list.contains("sun_4"))
        {
            sun_4.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_4"))
            {
                sun_4.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_4.setVisibility(View.INVISIBLE);
        }
        sun_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sun_4.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_4";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_4;

                }
            }
        });



        // row 4

        mon_5=(TextView)view.findViewById(R.id.mon_5);
        tue_5=(TextView)view.findViewById(R.id.tue_5);
        wed_5=(TextView)view.findViewById(R.id.wed_5);
        Th1_5=(TextView)view.findViewById(R.id.Th1_5);
        fri_5=(TextView)view.findViewById(R.id.fri_5);
        sat_5=(TextView)view.findViewById(R.id.sat_5);
        sun_5=(TextView)view.findViewById(R.id.sun_5);

        if(monay_list.contains("mon_5"))
        {
            mon_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_5"))
            {
                mon_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_5.setVisibility(View.INVISIBLE);
        }
        mon_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_5;

                }

            }
        });
        if(tuesday_list.contains("tue_5"))
        {
            tue_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_5"))
            {
                tue_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_5.setVisibility(View.INVISIBLE);
        }
        tue_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_5;

                }

            }
        });
        if(wednesday_list.contains("wed_5"))
        {
            wed_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_5"))
            {
                wed_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_5.setVisibility(View.INVISIBLE);
        }
        wed_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_5;

                }

            }
        });
        if(thursday_list.contains("Th1_5"))
        {
            Th1_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_5"))
            {
                Th1_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_5.setVisibility(View.INVISIBLE);
        }
        Th1_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_5;

                }

            }
        });
        if(friday_list.contains("fri_5"))
        {
            fri_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_5"))
            {
                Th1_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_5.setVisibility(View.INVISIBLE);
        }
        fri_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_5;

                }

            }
        });
        if(saturday_list.contains("sat_5"))
        {
            sat_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_5"))
            {
                Th1_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_5.setVisibility(View.INVISIBLE);
        }
        sat_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();


                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_5;

                }
            }
        });
        if(sunday_list.contains("sun_5"))
        {
            sun_5.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_5"))
            {
                sun_5.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_5.setVisibility(View.INVISIBLE);
        }
        sun_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sun_5.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_5";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_5;

                }

            }
        });



        //

        mon_7=(TextView)view.findViewById(R.id.mon_7);
        tue_7=(TextView)view.findViewById(R.id.tue_7);
        wed_7=(TextView)view.findViewById(R.id.wed_7);
        Th1_7=(TextView)view.findViewById(R.id.Th1_7);
        fri_7=(TextView)view.findViewById(R.id.fri_7);
        sat_7=(TextView)view.findViewById(R.id.sat_7);
        sun_7=(TextView)view.findViewById(R.id.sun_7);

        if(monay_list.contains("mon_7"))
        {
            mon_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_7"))
            {
                mon_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_7.setVisibility(View.INVISIBLE);
        }
        mon_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_7;

                }
            }
        });
        if(tuesday_list.contains("tue_7"))
        {
            tue_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_7"))
            {
                tue_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_7.setVisibility(View.INVISIBLE);
        }
        tue_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_7;

                }

            }
        });
        if(wednesday_list.contains("wed_7"))
        {
            wed_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_7"))
            {
                wed_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_7.setVisibility(View.INVISIBLE);
        }
        wed_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_7;

                }

            }
        });
        if(thursday_list.contains("Th1_7"))
        {
            Th1_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_7"))
            {
                Th1_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_7.setVisibility(View.INVISIBLE);
        }
        Th1_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_7;

                }

            }
        });
        if(friday_list.contains("fri_7"))
        {
            fri_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_7"))
            {
                fri_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_7.setVisibility(View.INVISIBLE);
        }
        fri_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_7;

                }

            }
        });
        if(saturday_list.contains("sat_7"))
        {
            sat_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_7"))
            {
                sat_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_7.setVisibility(View.INVISIBLE);
        }
        sat_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();


                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_7;

                }
            }
        });
        if(sunday_list.contains("sun_7"))
        {
            sun_7.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_7"))
            {
                sun_7.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_7.setVisibility(View.INVISIBLE);
        }
        sun_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sun_7.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_7";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_7;

                }
            }
        });

        //


        mon_8=(TextView)view.findViewById(R.id.mon_8);
        tue_8=(TextView)view.findViewById(R.id.tue_8);
        wed_8=(TextView)view.findViewById(R.id.wed_8);
        Th1_8=(TextView)view.findViewById(R.id.Th1_8);
        fri_8=(TextView)view.findViewById(R.id.fri_8);
        sat_8=(TextView)view.findViewById(R.id.sat_8);
        sun_8=(TextView)view.findViewById(R.id.sun_8);

        if(monay_list.contains("mon_8"))
        {
            mon_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_8"))
            {
                mon_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_8.setVisibility(View.INVISIBLE);
        }
        mon_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_8;

                }
            }
        });
        if(tuesday_list.contains("tue_8"))
        {
            tue_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_8"))
            {
                tue_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_8.setVisibility(View.INVISIBLE);
        }
        tue_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_8;

                }
            }
        });
        if(wednesday_list.contains("wed_8"))
        {
            wed_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_8"))
            {
                wed_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_8.setVisibility(View.INVISIBLE);
        }
        wed_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_8;

                }
            }
        });
        if(thursday_list.contains("Th1_8"))
        {
            Th1_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_8"))
            {
                Th1_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_8.setVisibility(View.INVISIBLE);
        }
        Th1_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_8;

                }
            }
        });
        if(friday_list.contains("fri_8"))
        {
            fri_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_8"))
            {
                fri_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_8.setVisibility(View.INVISIBLE);
        }
        fri_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_8;

                }
            }
        });
        if(saturday_list.contains("sat_8"))
        {
            sat_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_8"))
            {
                sat_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_8.setVisibility(View.INVISIBLE);
        }
        sat_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_8;

                }
            }
        });
        if(sunday_list.contains("sun_8"))
        {
            sun_8.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_8"))
            {
                sun_8.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_8.setVisibility(View.INVISIBLE);
        }
        sun_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sun_8.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_8";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_8;

                }
            }
        });

        //

        mon_9=(TextView)view.findViewById(R.id.mon_9);
        tue_9=(TextView)view.findViewById(R.id.tue_9);
        wed_9=(TextView)view.findViewById(R.id.wed_9);
        Th1_9=(TextView)view.findViewById(R.id.Th1_9);
        fri_9=(TextView)view.findViewById(R.id.fri_9);
        sat_9=(TextView)view.findViewById(R.id.sat_9);
        sun_9=(TextView)view.findViewById(R.id.sun_9);

        if(monay_list.contains("mon_9"))
        {
            mon_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_9"))
            {
                mon_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_9.setVisibility(View.INVISIBLE);
        }
        mon_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_9;

                }
            }
        });
        if(tuesday_list.contains("tue_9"))
        {
            tue_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_9"))
            {
                tue_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_9.setVisibility(View.INVISIBLE);
        }
        tue_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_9;

                }
            }
        });
        if(wednesday_list.contains("wed_9"))
        {
            wed_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_9"))
            {
                wed_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_9.setVisibility(View.INVISIBLE);
        }
        wed_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_9;

                }
            }
        });
        if(thursday_list.contains("Th1_9"))
        {
            Th1_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_9"))
            {
                Th1_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_9.setVisibility(View.INVISIBLE);
        }
        Th1_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                Th1_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_9;

                }
            }
        });
        if(friday_list.contains("fri_9"))
        {
            fri_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_9"))
            {
                fri_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_9.setVisibility(View.INVISIBLE);
        }
        fri_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fri_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_9;

                }
            }
        });
        if(saturday_list.contains("sat_9"))
        {
            sat_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_9"))
            {
                sat_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_9.setVisibility(View.INVISIBLE);
        }
        sat_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_9;

                }
            }
        });
        if(sunday_list.contains("sun_9"))
        {
            sun_9.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_9"))
            {
                sun_9.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_9.setVisibility(View.INVISIBLE);
        }
        sun_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sun_9.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_9";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_9;

                }
            }
        });

        //

        mon_10=(TextView)view.findViewById(R.id.mon_10);
        tue_10=(TextView)view.findViewById(R.id.tue_10);
        wed_10=(TextView)view.findViewById(R.id.wed_10);
        Th1_10=(TextView)view.findViewById(R.id.Th1_10);
        fri_10=(TextView)view.findViewById(R.id.fri_10);
        sat_10=(TextView)view.findViewById(R.id.sat_10);
        sun_10=(TextView)view.findViewById(R.id.sun_10);

        if(monay_list.contains("mon_10"))
        {
            mon_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_10"))
            {
                mon_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_10.setVisibility(View.INVISIBLE);
        }
        mon_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_10;

                }
            }
        });
        if(tuesday_list.contains("tue_10"))
        {
            tue_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_10"))
            {
                tue_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_10.setVisibility(View.INVISIBLE);
        }
        tue_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                tue_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_10;

                }
            }
        });
        if(wednesday_list.contains("wed_10"))
        {
            wed_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_10"))
            {
                wed_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_10.setVisibility(View.INVISIBLE);
        }
        wed_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_10;

                }
            }
        });
        if(thursday_list.contains("Th1_10"))
        {
            Th1_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_10"))
            {
                Th1_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_10.setVisibility(View.INVISIBLE);
        }
        Th1_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_10;

                }
            }
        });
        if(friday_list.contains("fri_10"))
        {
            fri_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_10"))
            {
                fri_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_10.setVisibility(View.INVISIBLE);
        }
        fri_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_10;

                }
            }
        });
        if(saturday_list.contains("sat_10"))
        {
            sat_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_10"))
            {
                sat_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_10.setVisibility(View.INVISIBLE);
        }
        sat_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sat_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_10;

                }
            }
        });
        if(sunday_list.contains("sun_10"))
        {
            sun_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_10"))
            {
                sun_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_10.setVisibility(View.INVISIBLE);
        }
        sun_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                sun_10.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_10";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_10;

                }
            }
        });

        //

        mon_11=(TextView)view.findViewById(R.id.mon_11);
        tue_11=(TextView)view.findViewById(R.id.tue_11);
        wed_11=(TextView)view.findViewById(R.id.wed_11);
        Th1_11=(TextView)view.findViewById(R.id.Th1_11);
        fri_11=(TextView)view.findViewById(R.id.fri_11);
        sat_11=(TextView)view.findViewById(R.id.sat_11);
        sun_11=(TextView)view.findViewById(R.id.sun_11);

        if(monay_list.contains("mon_11"))
        {
            mon_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_11"))
            {
                mon_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_11.setVisibility(View.INVISIBLE);
        }
        mon_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mon_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_11;

                }
            }
        });
        if(tuesday_list.contains("tue_11"))
        {
            tue_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_11"))
            {
                tue_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_11.setVisibility(View.INVISIBLE);
        }
        tue_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_11;

                }
            }
        });
        if(wednesday_list.contains("wed_11"))
        {
            wed_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_11"))
            {
                wed_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_11.setVisibility(View.INVISIBLE);
        }
        wed_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_11;

                }
            }
        });
        if(thursday_list.contains("Th1_11"))
        {
            Th1_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_11"))
            {
                Th1_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_11.setVisibility(View.INVISIBLE);
        }
        Th1_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_11;

                }
            }
        });
        if(friday_list.contains("fri_11"))
        {
            fri_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_11"))
            {
                fri_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_11.setVisibility(View.INVISIBLE);
        }
        fri_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fri_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_11;

                }
            }
        });
        if(saturday_list.contains("sat_11"))
        {
            sat_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_11"))
            {
                sat_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_11.setVisibility(View.INVISIBLE);
        }
        sat_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_11;

                }
            }
        });
        if(sunday_list.contains("sun_11"))
        {
            sun_11.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_11"))
            {
                sun_11.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_11.setVisibility(View.INVISIBLE);
        }
        sun_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                sun_11.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_11";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_11;

                }
            }
        });

        //

        mon_12=(TextView)view.findViewById(R.id.mon_12);
        tue_12=(TextView)view.findViewById(R.id.tue_12);
        wed_12=(TextView)view.findViewById(R.id.wed_12);
        Th1_12=(TextView)view.findViewById(R.id.Th1_12);
        fri_12=(TextView)view.findViewById(R.id.fri_12);
        sat_12=(TextView)view.findViewById(R.id.sat_12);
        sun_12=(TextView)view.findViewById(R.id.sun_12);

        if(monay_list.contains("mon_12"))
        {
            mon_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_12"))
            {
                mon_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_12.setVisibility(View.INVISIBLE);
        }
        mon_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mon_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_12;

                }
            }
        });
        if(tuesday_list.contains("tue_12"))
        {
            tue_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_12"))
            {
                tue_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_12.setVisibility(View.INVISIBLE);
        }
        tue_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_12;

                }
            }
        });
        if(wednesday_list.contains("wed_12"))
        {
            wed_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_12"))
            {
                wed_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_12.setVisibility(View.INVISIBLE);
        }
        wed_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_12;

                }
            }
        });
        if(thursday_list.contains("Th1_12"))
        {
            Th1_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_12"))
            {
                Th1_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_12.setVisibility(View.INVISIBLE);
        }
        Th1_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_12;

                }
            }
        });
        if(friday_list.contains("fri_12"))
        {
            fri_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_12"))
            {
                fri_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_12.setVisibility(View.INVISIBLE);
        }
        fri_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_12;

                }
            }
        });
        if(saturday_list.contains("sat_12"))
        {
            sat_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_12"))
            {
                sat_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_12.setVisibility(View.INVISIBLE);
        }
        sat_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_12;

                }
            }
        });
        if(sunday_list.contains("sun_12"))
        {
            sun_12.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_12"))
            {
                sun_12.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_12.setVisibility(View.INVISIBLE);
        }
        sun_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sun_12.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_12";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_12;

                }
            }
        });

        //

        mon_13=(TextView)view.findViewById(R.id.mon_13);
        tue_13=(TextView)view.findViewById(R.id.tue_13);
        wed_13=(TextView)view.findViewById(R.id.wed_13);
        Th1_13=(TextView)view.findViewById(R.id.Th1_13);
        fri_13=(TextView)view.findViewById(R.id.fri_13);
        sat_13=(TextView)view.findViewById(R.id.sat_13);
        sun_13=(TextView)view.findViewById(R.id.sun_13);

        if(monay_list.contains("mon_13"))
        {
            mon_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_13"))
            {
                mon_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_13.setVisibility(View.INVISIBLE);
        }
        mon_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_13;

                }
            }
        });
        if(tuesday_list.contains("tue_13"))
        {
            tue_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_13"))
            {
                tue_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_13.setVisibility(View.INVISIBLE);
        }
        tue_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_13;

                }
            }
        });
        if(wednesday_list.contains("wed_13"))
        {
            wed_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_13"))
            {
                wed_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_13.setVisibility(View.INVISIBLE);
        }
        wed_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_13;

                }

            }
        });
        if(thursday_list.contains("Th1_13"))
        {
            Th1_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_13"))
            {
                Th1_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_13.setVisibility(View.INVISIBLE);
        }
        Th1_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_13;

                }

            }
        });
        if(friday_list.contains("fri_13"))
        {
            fri_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_13"))
            {
                fri_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_13.setVisibility(View.INVISIBLE);
        }
        fri_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_13;

                }

            }
        });
        if(saturday_list.contains("sat_13"))
        {
            sat_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_13"))
            {
                sat_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_13.setVisibility(View.INVISIBLE);
        }
        sat_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_13;

                }
            }
        });
        if(sunday_list.contains("sun_13"))
        {
            sun_13.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_13"))
            {
                sun_13.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_13.setVisibility(View.INVISIBLE);
        }
        sun_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sun_13.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_13";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_13;

                }
            }
        });

        //

        mon_14=(TextView)view.findViewById(R.id.mon_14);
        tue_14=(TextView)view.findViewById(R.id.tue_14);
        wed_14=(TextView)view.findViewById(R.id.wed_14);
        Th1_14=(TextView)view.findViewById(R.id.Th1_14);
        fri_14=(TextView)view.findViewById(R.id.fri_14);
        sat_14=(TextView)view.findViewById(R.id.sat_14);
        sun_14=(TextView)view.findViewById(R.id.sun_14);

        if(monay_list.contains("mon_14"))
        {
            mon_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_14"))
            {
                mon_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_14.setVisibility(View.INVISIBLE);
        }
        mon_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                mon_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_14;

                }
            }
        });
        if(tuesday_list.contains("tue_14"))
        {
            tue_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_14"))
            {
                tue_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_14.setVisibility(View.INVISIBLE);
        }
        tue_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_14;

                }
            }
        });
        if(wednesday_list.contains("wed_14"))
        {
            wed_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_14"))
            {
                wed_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_14.setVisibility(View.INVISIBLE);
        }
        wed_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_14;

                }
            }
        });
        if(thursday_list.contains("Th1_14"))
        {
            Th1_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_14"))
            {
                Th1_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_14.setVisibility(View.INVISIBLE);
        }
        Th1_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_14;

                }
            }
        });
        if(friday_list.contains("fri_14"))
        {
            fri_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_14"))
            {
                fri_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_14.setVisibility(View.INVISIBLE);
        }
        fri_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_14;

                }
            }
        });
        if(saturday_list.contains("sat_14"))
        {
            sat_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_14"))
            {
                sat_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_14.setVisibility(View.INVISIBLE);
        }
        sat_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sat_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_14;

                }
            }
        });
        if(sunday_list.contains("sun_14"))
        {
            sun_14.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_14"))
            {
                sun_14.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_14.setVisibility(View.INVISIBLE);
        }
        sun_14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sun_14.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sun_14";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_14;

                }
            }
        });

        //

        mon_15=(TextView)view.findViewById(R.id.mon_15);
        tue_15=(TextView)view.findViewById(R.id.tue_15);
        wed_15=(TextView)view.findViewById(R.id.wed_15);
        Th1_15=(TextView)view.findViewById(R.id.Th1_15);
        fri_15=(TextView)view.findViewById(R.id.fri_15);
        sat_15=(TextView)view.findViewById(R.id.sat_15);
        sun_15=(TextView)view.findViewById(R.id.sun_15);

        if(monay_list.contains("mon_15"))
        {
            mon_15.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("mon_15"))
            {
                mon_15.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            mon_15.setVisibility(View.INVISIBLE);
        }
        mon_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mon_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"mon_15";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=mon_15;

                }
            }
        });
        if(tuesday_list.contains("tue_15"))
        {
            tue_15.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("tue_15"))
            {
                tue_15.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            tue_15.setVisibility(View.INVISIBLE);
        }
        tue_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tue_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"tue_15";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=tue_15;

                }
            }
        });
        if(wednesday_list.contains("wed_15"))
        {
            wed_15.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("wed_15"))
            {
                wed_15.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            wed_15.setVisibility(View.INVISIBLE);
        }
        wed_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wed_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"wed_15";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=wed_15;

                }
            }
        });
        if(thursday_list.contains("Th1_15"))
        {
            Th1_10.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("Th1_10"))
            {
                Th1_10.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Th1_15.setVisibility(View.INVISIBLE);
        }
        Th1_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Th1_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"Th1_15";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=Th1_15;

                }
            }
        });
        if(friday_list.contains("fri_15"))
        {
            fri_15.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("fri_15"))
            {
                fri_15.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            fri_15.setVisibility(View.INVISIBLE);
        }
        fri_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fri_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"fri_15";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=fri_15;

                }
            }
        });
        if(saturday_list.contains("sat_15"))
        {
            sat_15.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sat_15"))
            {
                sat_15.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sat_15.setVisibility(View.INVISIBLE);
        }
        sat_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sat_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                appointments=appointments+"sat_15";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sat_15;

                }
            }
        });
        if(sunday_list.contains("sun_15"))
        {
            sun_15.setBackgroundResource(R.drawable.filled_box);
            if(appointent_selected_already.contains("sun_15"))
            {
                sun_15.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            sun_15.setVisibility(View.INVISIBLE);
        }
        sun_15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                sun_15.setBackgroundResource(R.drawable.taken_box);
                appointments="";
                Toast.makeText(getActivity(),"Your Appointment is selected",Toast.LENGTH_LONG).show();
                appointments=appointments+"sun_15";

                if(selectd!=null)
                {
                    selectd.setBackgroundResource(R.drawable.filled_box);
                    selectd=sun_15;

                }
            }
        });

    }

    public Notification get_notification_object()
    {

        Notification notification = new Notification();

        if(check_if_day_is_valid(notification)!=null)
        {
            notification=check_if_day_is_valid(notification);

            notification.setSendernmae(Information.loged_in_user.getUser_name());
            notification.setRecievername(Information.selected_doctor.getUser_name());
            notification.setRecievernumber(Information.selected_doctor.getUsers_phone_number());
            notification.setSendernumber(Information.loged_in_user.getUsers_phone_number());
            notification.setSenderlatitude(Information.loged_in_user.getLatitude());
            notification.setRecieverlatitude(Information.selected_doctor.getLatitude());
            notification.setSenderlongitutde(Information.loged_in_user.getLongitutude());
            notification.setRecieverlongitutde(Information.selected_doctor.getLongitutude());
            notification.setMesssage(Information.loged_in_user.getUser_name() +" wants to fix Appointment on"+appointments);
            notification.setSender_email(Information.loged_in_user.getUsers_email_id());
            notification.setReciever_email(Information.selected_doctor.getUsers_email_id());
            notification.setSenderpicurl(Information.loged_in_user.getProfile_photo());
            notification.setRecieverpicurl(Information.selected_doctor.getProfile_photo());
            notification.setSender_appid(Information.loged_in_user.getApp_id());
            notification.setReciever_appid(Information.selected_doctor.getApp_id());
            //notification.setTime("1200");
        }

        else
        {
            notification=null;
        }






        return notification;

    }

    public Notification check_if_day_is_valid(Notification notification)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        Log.d("current_day",dayOfTheWeek);
        TextView mon_1,tue_1,wed_1,Th1_1,fri_1,sat_1,sun_1;

        if(appointments.contains("tue") && (dayOfTheWeek.equals("Wednesday") || dayOfTheWeek.equals("Thursday") || dayOfTheWeek.equals("Friday") || dayOfTheWeek.equals("Saturday") || dayOfTheWeek.equals("Sunday")))
        {
            Toast.makeText(getActivity(),"You cant select previous Appointment",Toast.LENGTH_LONG).show();
            return null;
        }

       else if(appointments.contains("wed") && ( dayOfTheWeek.equals("Thursday") || dayOfTheWeek.equals("Friday") || dayOfTheWeek.equals("Saturday") || dayOfTheWeek.equals("Sunday")))
        {
            Toast.makeText(getActivity(),"You cant select previous Appointment",Toast.LENGTH_LONG).show();
            return null;
        }

       else if(appointments.contains("Th") && ( dayOfTheWeek.equals("Friday") || dayOfTheWeek.equals("Saturday") || dayOfTheWeek.equals("Sunday")))
        {
            Toast.makeText(getActivity(),"You cant select previous Appointment",Toast.LENGTH_LONG).show();
            return null;
        }

       else if(appointments.contains("fri") && ( dayOfTheWeek.equals("Saturday") || dayOfTheWeek.equals("Sunday")))
        {
            Toast.makeText(getActivity(),"You cant select previous Appointment",Toast.LENGTH_LONG).show();
            return null;
        }
        else if(appointments.contains("sat") && (dayOfTheWeek.equals("Sunday")))
        {
            Toast.makeText(getActivity(),"You cant select previous Appointment",Toast.LENGTH_LONG).show();
            return null;
        }

        else
        {
            String time;
            String date;

            Calendar instance = Calendar.getInstance();
            String currentMonth = instance.get(Calendar.MONTH)+"";
            String currentYear = instance.get(Calendar.YEAR)+"";

            notification.setDate("May 16,2016");
            notification.setTime("6:30PM");


        }

        return notification;
    }

    public void set_time(String appointment_selected)
    {

        if(appointment_selected.contains("_1"))
        {
            time="08:00 AM";
        }

        if(appointment_selected.contains("_2"))
        {
            time="09:00 AM";
        }

        if(appointment_selected.contains("_4"))
        {
            time="10:00 AM";
        }

        if(appointment_selected.contains("_5"))
        {
            time="11:00 AM";
        }
        if(appointment_selected.contains("_7"))
        {
            time="12:00 PM";
        }
        if(appointment_selected.contains("_8"))
        {
            time="01:00 PM";
        }
        if(appointment_selected.contains("_9"))
        {
            time="02:00 PM";
        }
        if(appointment_selected.contains("_10"))
        {
            time="03:00 Am";
        }
        if(appointment_selected.contains("_11"))
        {
            time="04:00 Am";
        }
        if(appointment_selected.contains("_12"))
        {
            time="05:00 Am";
        }
        if(appointment_selected.contains("_13"))
        {
            time="06:00 Am";
        }
        if(appointment_selected.contains("_14"))
        {
            time="07:00 Am";
        }
        if(appointment_selected.contains("_15"))
        {
            time="08:00 Am";
        }
    }

    public void set_dae(String appointment_selected)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);

        Calendar calendar = Calendar.getInstance();
        //int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        SimpleDateFormat month_date = new SimpleDateFormat("MMMM", Locale.US);

        date="12 Mar 2019";

    }

    @Override
    public void finalizing_appointmnet(String date, String time, String disese,String condition)
    {
        Notification notification = get_notification_object();

        if(notification!=null)
        {
            notification.setTime(time);
            notification.setDate(date);
            notification.setDesease(disese);
            notification.setStatus("request");
            notification.setReciever_city(Information.selected_doctor.getCity());
            notification.setSender_city(Information.loged_in_user.getCity());
            notification.setPateint_condition(condition);
            notification.setSendernumber(Information.loged_in_user.getUsers_phone_number());
            notification.setRecievernumber(Information.selected_doctor.getClinic_name());
            notification.setGender(Information.loged_in_user.getGender());


            appointent_selected_already=appointent_selected_already+","+appointments;

            Log.d("appointments",appointments);

            DatabaseReference database = FirebaseDatabase.getInstance().getReference("online_doctor");
            database.child("appointments_of_this_week").child((Information.selected_doctor.getUsers_email_id().replace(".","")).replace("@","")).setValue(new Appointments_of_doctor(appointent_selected_already));

            database.child("Notifications").child((Information.selected_doctor.getUsers_email_id().replace(".","")).replace("@","")).child(notification.getSendernmae()+notification.getTime()).setValue(notification);
            notification.setStatus("Pending");
            database.child("Notifications").child((Information.loged_in_user.getUsers_email_id().replace(".","")).replace("@","")).child(notification.getRecievername()+notification.getTime()).setValue(notification);
        }
        else
        {
            Toast.makeText(getActivity(),"Kindly select wisely",Toast.LENGTH_LONG).show();
        }

    }
}
