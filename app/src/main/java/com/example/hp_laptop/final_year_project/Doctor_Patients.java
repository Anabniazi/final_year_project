package com.example.hp_laptop.final_year_project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Doctor_Patients extends AppCompatActivity {

    ArrayList<patient> patients=null;
    Doctors_pateint_adapter adapter=null;
    ListView theListView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor__patients);

        theListView = findViewById(R.id.mainListView);
        getPateintsList();


        theListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                patient patient=patients.get(position);

                Intent intent = new Intent(Doctor_Patients.this,patient_history.class);
                intent.putExtra("selected_pateint",patient);
                startActivity(intent);

            }

        });

    }

    private void getPateintsList()
    {
        patients = new ArrayList<>();

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("pateints").child((Information.loged_in_user.getUsers_email_id().replace(".","")).replace("@",""));
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot

                        for (DataSnapshot dsp : dataSnapshot.getChildren())
                        {
                            patient patient = dsp.getValue(patient.class);
                            patients.add(patient);
                        }

                        adapter = new Doctors_pateint_adapter(getApplicationContext(), patients);
                        theListView.setAdapter(adapter);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });

    }
}
