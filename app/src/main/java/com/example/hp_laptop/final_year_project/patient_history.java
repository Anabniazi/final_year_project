package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.PrecomputedText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ramotion.foldingcell.FoldingCell;
import com.scalified.fab.ActionButton;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class patient_history extends AppCompatActivity {

    patient patient;
    ActionButton add_button;
    LinearLayout main_layout;
    com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pateints_prescritions); // first it was activity_patient_history

        next =(com.barnettwong.dragfloatactionbuttonlibrary.view.DragFloatActionButton)findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                Intent intent = new Intent(patient_history.this,Medical_form.class);
                intent.putExtra("selected_pateint",patient);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
         patient = intent.getParcelableExtra("selected_pateint");

        main_layout=(LinearLayout)findViewById(R.id.outer_layout);
        download_prescriptions();





    }

    public void download_prescriptions()
    {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("prescriptions").child((patient.getEmail().replace(".","")).replace("@",""));
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot

                        Log.d("amountt",dataSnapshot.getChildrenCount()+"");

                        for (DataSnapshot dsp : dataSnapshot.getChildren())
                        {
                            Prescription notification = dsp.getValue(Prescription.class);
                            //Log.d("dato_-1",notification+" !=null");
                            Log.d("dato",notification.getDate() +" !=null");
                            Log.d("dato_0",notification.getMedi_list() +" !=null");//*

                            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View rowView = inflater.inflate(R.layout.patinet_field, null);

                            EditText head_image_left_text =(EditText) rowView.findViewById(R.id.head_image_left_text);
                            head_image_left_text.setText(notification.getTotal_days());

                            TextView content_name_view=(TextView)rowView.findViewById(R.id.content_name_view);
                            content_name_view.setText(notification.getDoctor_name());

                            TextView date = (TextView)rowView.findViewById(R.id.date);
                            date.setText(notification.getDate());

                            LinearLayout scrollView=(LinearLayout)rowView.findViewById(R.id.inner_layout);
                            main_layout.addView(rowView, main_layout.getChildCount() - 1);

                            ImageView imageView=(ImageView)rowView.findViewById(R.id.content_avatar);
                            Picasso.with(patient_history.this).load(notification.getDoctor_pic()).placeholder(R.drawable.de_profile_image).into(imageView);
                            //TextView content_name_view1 = (TextView)rowView.findViewById(R.id.content_name_view);
                            //content_name_view1.setText(notification.getDoctor_name());

                            /*LayoutInflater inflaters = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            final View rowViews = inflaters.inflate(R.layout.field, null);
                            scrollView.addView(rowViews, scrollView.getChildCount() - 1);


*/
                            if(notification.getMedi_list()!=null)
                            {


                            for(int i =0; i<notification.getMedi_list().size();i++)
                            {
                                Medicine medicine = notification.medi_list.get(i);
                                LayoutInflater inflaters = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View rowViews = inflaters.inflate(R.layout.field, null);
                                scrollView.addView(rowViews, scrollView.getChildCount() - 1);

                                EditText content_delivery_time = (EditText) rowViews.findViewById(R.id.content_delivery_time);
                                content_delivery_time.setText(notification.getMedi_list().get(i).getTime());
                                disableEditText(content_delivery_time);
                                String[] array_time=notification.getMedi_list().get(i).getTime().split(":");


                                EditText content_delivery_date =(EditText) rowViews.findViewById(R.id.content_delivery_date);
                                content_delivery_date.setText(notification.getMedi_list().get(i).getDate());
                                disableEditText(content_delivery_date);
                                EditText time_limit =(EditText) rowViews.findViewById(R.id.time_limit);
                                time_limit.setText(notification.getMedi_list().get(i).getLimit());
                                disableEditText(time_limit);

                                CheckBox morn = (CheckBox) rowViews.findViewById(R.id.morn);
                                morn.setChecked(notification.getMedi_list().get(i).isMon());
                                morn.setEnabled(false);
                                CheckBox lunch = (CheckBox) rowViews.findViewById(R.id.lunch);
                                lunch.setChecked(notification.getMedi_list().get(i).isTue());
                                lunch.setEnabled(false);
                                CheckBox dinner = (CheckBox) rowViews.findViewById(R.id.dinner);
                                dinner.setChecked(notification.getMedi_list().get(i).isWed());
                                dinner.setEnabled(false);

                            }
                            }

                        }
                        Log.d("amountt",dataSnapshot.getChildrenCount()+" aaaaaaa");



                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

}
