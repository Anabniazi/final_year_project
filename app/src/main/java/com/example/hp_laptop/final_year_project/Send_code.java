package com.example.hp_laptop.final_year_project;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.scalified.fab.ActionButton;


public class Send_code extends DialogFragment {

    com.chaos.view.PinView pin;
    ActionButton phone_number_entered;
    ActionButton code_entered;
    ActionButton reenter_code;
    EditText phone_number;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.activity_send_code,container,false);

        if (getDialog() != null && getDialog().getWindow() != null) {
            // getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        setCancelable(false);

        this.phone_number=  (EditText) view.findViewById(R.id.Phone_number);
        pin=(com.chaos.view.PinView) view.findViewById(R.id.firstPinView);
        phone_number_entered =(ActionButton)view.findViewById(R.id.number_entered);
        reenter_code =(ActionButton)view.findViewById(R.id.reenter_code);
        code_entered =(ActionButton)view.findViewById(R.id.code_entered);



        phone_number_entered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //    startPhoneNumberVerification(phone_number.getText().toString());
            }
        });

        code_entered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                pin.getText();

             //   verifyPhoneNumberWithCode(pin.getText()+"",mvarificationid);
                dismiss();
            }
        });


        reenter_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {


            //    resendVerificationCode(phone_number.getText().toString(),mforceResendingToken);
            }
        });


        return view;
    }
}
