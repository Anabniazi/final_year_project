package com.example.hp_laptop.final_year_project;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class Information
{
    //login user information

    public static String user_name="";
    public static String users_phone_number="";
    public static String users_email_id="";
    public static String users_Profession="patience";
    public static String users_image="";
    public static String doctor_or_patient="patient";
    public static String Address="";
    public static String clinic_name="";
    public static String specialization="";
    public static String cnic="";
    public static String pmdc="";
    public static String gender="male";
    public static String provider="";
    public static String city="Mianwali";
    public static String latitude="25.555";
    public static String longitutude="25.00";
    public static String experience="";
    public static String clnic_number="";

    public static Uri selected_image_Uri=null;
    public static String selected_image_Url;
    public static String email="";
    public static String current_loged_in_user_email="";
    public  static Bitmap profile_photo;


    public static List<User> same_city_users = new ArrayList<>();
    public static User loged_in_user;

    public static User selected_doctor;
}
