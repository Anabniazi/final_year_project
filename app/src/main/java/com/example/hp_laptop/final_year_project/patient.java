package com.example.hp_laptop.final_year_project;

import android.os.Parcel;
import android.os.Parcelable;

public class patient implements Parcelable
{
    private String name;
    private String number;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    private String email;
    private String image;
    private  String city;
    private  String address;
    private  String cnic;
    private String gender;
    private String time;
    private String date;
    private String disese;
    private String status;
    private String appid;

    public patient()
    {

    }
    public patient( String name, String number, String email, String image,String cnic,String gender,String city,String address,String date,String time,String disease,String status,String appid)
    {
        this.name=name;
        this.email=email;
        this.image=image;
        this.number=number;
        this.city=city;
        this.address=address;
        this.cnic=cnic;
        this.gender=gender;
        this.date=date;
        this.time=time;
        this.disese=disease;
        this.status=status;
        this.appid=appid;
    }

    protected patient(Parcel in) {
        name = in.readString();
        number = in.readString();
        email = in.readString();
        image = in.readString();
        city = in.readString();
        address = in.readString();
        cnic = in.readString();
        gender = in.readString();
        time = in.readString();
        date = in.readString();
        disese = in.readString();
        status = in.readString();
        appid= in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(email);
        dest.writeString(image);
        dest.writeString(city);
        dest.writeString(address);
        dest.writeString(cnic);
        dest.writeString(gender);
        dest.writeString(time);
        dest.writeString(date);
        dest.writeString(disese);
        dest.writeString(status);
        dest.writeString(appid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<patient> CREATOR = new Creator<patient>() {
        @Override
        public patient createFromParcel(Parcel in) {
            return new patient(in);
        }

        @Override
        public patient[] newArray(int size) {
            return new patient[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public  String getCity() {
        return city;
    }

    public  void setCity(String city) {
        this.city = city;
    }

    public  String getAddress() {
        return address;
    }

    public  void setAddress(String address) {
        this.address = address;
    }

    public  String getCnic() {
        return cnic;
    }

    public  void setCnic(String cnic) {
        this.cnic = cnic;
    }

    public  String getGender() {
        return gender;
    }

    public  void setGender(String gender) {
        this.gender = gender;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDisese() {
        return disese;
    }

    public void setDisese(String disese) {
        this.disese = disese;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
