package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.scalified.fab.ActionButton;

import java.io.IOException;

import static com.example.hp_laptop.final_year_project.Dailogs.DismissLoading;
import static com.example.hp_laptop.final_year_project.Dailogs.Loading;

public class Asisstant_Activity extends AppCompatActivity {

    ActionButton next;
    ActionButton more_assisstant;
    ActionButton location_button;

    EditText username;
    EditText email;
    EditText phone;
    EditText cnic;

    Uri profile_pic;
    String usernmae;
    String useremail;
    String phonenumber;
    String usercnic;


    com.pkmmte.view.CircularImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asisstant_);

        imageView= (com.pkmmte.view.CircularImageView) findViewById(R.id.profilePic);
        username=(EditText)findViewById(R.id.user_name);
        email=(EditText)findViewById(R.id.user_email);
        phone=(EditText)findViewById(R.id.user_phone_number);
        cnic=(EditText)findViewById(R.id.user_cnic);







        next=(ActionButton)findViewById(R.id.add_new);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                User user = new User();
                user.setUsers_email_id(email.getText().toString());
                user.setUser_name(username.getText().toString());
                user.setUsers_phone_number(phone.getText().toString());
                user.setCnic(cnic.getText().toString());


                save_image(user);

                username.setText("");
                email.setText("");
                phone.setText("");
                cnic.setText("");

                /*finish();
                Intent intent = new Intent(Asisstant_Activity.this,Asisstant_Activity.class);
                startActivity(intent);*/
            }
        });



        more_assisstant=(ActionButton)findViewById(R.id.add);
        more_assisstant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               /* User user = new User();
                user.setUsers_email_id(email.getText().toString());
                user.setUser_name(username.getText().toString());
                user.setUsers_phone_number(phone.getText().toString());
                user.setCnic(cnic.getText().toString());


                save_image(user);

                username.setText("");
                email.setText("");
                phone.setText("");
                cnic.setText("");*/

                Intent intent = new Intent(Asisstant_Activity.this,Doctor_HomeScreen.class);
                startActivity(intent);

            }
        });

        location_button=(ActionButton)findViewById(R.id.location_button);
        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"select files"),456);

            }
        });





    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK)
        {
            if(requestCode==456)
            {
                try {
                    Bitmap photo = MediaStore.Images.Media.getBitmap(this.getApplicationContext().getContentResolver(), data.getData());
                    profile_pic=data.getData();;
                    imageView.setImageBitmap(photo);
                    Log.i("tictak", photo + "    photo       " + imageView);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


    }




    public void save_user(User user)
    {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("online_doctor");
        database.child("Assistants").child((Information.current_loged_in_user_email.replace(".","")).replace("@","")).push().setValue(user);
    }



    public void save_image(final User user)
    {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();

        final StorageReference riversRef = storageRef.child("Usersprofilephotos").child((email.getText().toString().replace(".","")).replace("@",""));

        UploadTask uploadTask = riversRef.putFile(profile_pic);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful())
                {
                    DismissLoading(Asisstant_Activity.this);
                    throw task.getException();
                }


                return riversRef.getDownloadUrl();
            }

        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    DismissLoading(Asisstant_Activity.this);
                    user.setProfile_photo(task.getResult()+"");
                    save_user(user);
                    Log.d("downloadurl_1",task.getResult()+"");
                    // save_user(make_object_of_user());
                } else {
                    // Handle failures
                    // ...
                }
            }
        });


    }




}
