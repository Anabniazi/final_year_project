package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ramotion.foldingcell.FoldingCell;

import java.util.HashSet;
import java.util.List;





public class FoldingCellListAdapter_patient_history extends ArrayAdapter<Prescription>
{

    private HashSet<Integer> unfoldedIndexes = new HashSet<>();
    private View.OnClickListener defaultRequestBtnClickListener;
    Context context;

    public FoldingCellListAdapter_patient_history(Context context, List<Prescription> objects) {
        super(context, 0, objects);
        this.context=context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // get item for selected view
        Prescription item = getItem(position);
        // if cell is exists - reuse it, if not - create the new one from resource
        FoldingCell cell = (FoldingCell) convertView;
        ViewHolder viewHolder;
        if (cell == null) {
            viewHolder = new ViewHolder();
            LayoutInflater vi = LayoutInflater.from(getContext());
            cell = (FoldingCell) vi.inflate(R.layout.pateint_history_cell, parent, false);
            // binding view parts to view holder




            viewHolder.title_price = cell.findViewById(R.id.title_price);
            viewHolder.title_time_label = cell.findViewById(R.id.title_time_label);
            viewHolder.title_date_label = cell.findViewById(R.id.title_date_label);
            viewHolder.disaese = cell.findViewById(R.id.disaese);
            viewHolder.doctor_name = cell.findViewById(R.id.doctor_name);
            viewHolder.clinic_name = cell.findViewById(R.id.clinic_name);


            viewHolder.content_avatar=cell.findViewById(R.id.content_avatar);
            viewHolder.content_name_view=cell.findViewById(R.id.content_name_view);
            viewHolder.medicine_1=cell.findViewById(R.id.medicine_1);
            viewHolder.medicine_1_detail=cell.findViewById(R.id.medicine_1_detail);
            viewHolder.medicine_1_to_date=cell.findViewById(R.id.medicine_1_to_date);
            viewHolder.medicine_2=cell.findViewById(R.id.medicine_2);
            viewHolder.medicine_2_detail=cell.findViewById(R.id.medicine_2_detail);
            viewHolder.medicine_2_to_date=cell.findViewById(R.id.medicine_2_to_date);





            cell.setTag(viewHolder);
        } else {
            // for existing cell set valid valid state(without animation)
            if (unfoldedIndexes.contains(position)) {
                cell.unfold(true);
            } else {
                cell.fold(true);
            }
            viewHolder = (ViewHolder) cell.getTag();
        }

        if (null == item)
            return cell;



        // bind data from selected element to view through view holder

       /* viewHolder.title_price.setText(item.getTitle_price());
        viewHolder.title_time_label.setText(item.getTitle_time_label());
        viewHolder.title_date_label.setText(item.getTitle_date_label());
        viewHolder.disaese.setText(item.getDisaese());
        viewHolder.doctor_name.setText(item.getDoctor_name());
        viewHolder.clinic_name.setText(item.getClinic_name());


      //  viewHolder.content_avatar.setImageBitmap(null);
        viewHolder.content_name_view.setText(item.getContent_name_view());
        viewHolder.medicine_1.setText(item.getMedicine_1());
        viewHolder.medicine_1_detail.setText(item.getMedicine_1_detail());
        viewHolder.medicine_1_to_date.setText(item.getMedicine_1_to_date());
        viewHolder.medicine_2.setText(item.getMedicine_2());
        viewHolder.medicine_2_detail.setText(item.getMedicine_2_detail());
        viewHolder.medicine_2_to_date.setText(item.getMedicine_2_to_date());*/

        return cell;
    }

    // simple methods for register cell state changes
    public void registerToggle(int position) {
        if (unfoldedIndexes.contains(position))
            registerFold(position);
        else
            registerUnfold(position);
    }

    public void registerFold(int position) {
        unfoldedIndexes.remove(position);
    }

    public void registerUnfold(int position) {
        unfoldedIndexes.add(position);
    }

    public View.OnClickListener getDefaultRequestBtnClickListener() {
        return defaultRequestBtnClickListener;
    }

    public void setDefaultRequestBtnClickListener(View.OnClickListener defaultRequestBtnClickListener) {
        this.defaultRequestBtnClickListener = defaultRequestBtnClickListener;
    }

    // View lookup cache
    private static class ViewHolder {
        /*TextView price;
        TextView contentRequestBtn;
        TextView pledgePrice;
        TextView fromAddress;
        TextView toAddress;
        TextView requestsCount;
        TextView date;
        TextView time;
        TextView status;*/



        TextView title_price;
        TextView title_time_label;
        TextView title_date_label;
        TextView disaese;
        TextView doctor_name;
        TextView clinic_name;
        /*TextView title_status;
        TextView name;*/


        // content views



        ImageView content_avatar;
        TextView content_name_view;
        TextView medicine_1;
        TextView medicine_1_detail;
        TextView medicine_1_to_date;
        TextView medicine_2;
        TextView medicine_2_detail;
        TextView medicine_2_to_date;



    }



}
