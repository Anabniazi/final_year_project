package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.ramotion.foldingcell.FoldingCell;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.zip.Inflater;

public class Doctors_pateint_adapter extends BaseAdapter
{

    Context context;
    ArrayList<patient> pateints;
    LayoutInflater inflter;

    public Doctors_pateint_adapter(Context context, ArrayList<patient> objects) {

        this.context=context;
        pateints=objects;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return pateints.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        patient pateint = pateints.get(position);


            View view = inflter.inflate(R.layout.doctors_pateint_item_layout, parent, false);

        com.pkmmte.view.CircularImageView profileicon = view.findViewById(R.id.profilePic);

        TextView title_time_label =(TextView) view.findViewById(R.id.title_time_label) ;
        TextView title_date_label=(TextView) view.findViewById(R.id.title_date_label) ;
        TextView disaese=(TextView) view.findViewById(R.id.disaese) ;
        TextView location=(TextView) view.findViewById(R.id.location) ;
        TextView user_name=(TextView) view.findViewById(R.id.user_name) ;
        TextView title_status=(TextView) view.findViewById(R.id.title_status) ;
        TextView phone_number=(TextView) view.findViewById(R.id.phone_number) ;

        title_date_label.setText(pateint.getDate());
        title_time_label.setText(pateint.getTime());
        disaese.setText(pateint.getDisese());
        location.setText(pateint.getCity());
        user_name.setText(pateint.getName());
        title_status.setText(pateint.getStatus());
        phone_number.setText(pateint.getNumber());





        return view;
    }




}
