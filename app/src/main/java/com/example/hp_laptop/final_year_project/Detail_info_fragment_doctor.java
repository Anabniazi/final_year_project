package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.scalified.fab.ActionButton;
import com.squareup.picasso.Picasso;

import co.ceryle.radiorealbutton.RadioRealButtonGroup;


public class Detail_info_fragment_doctor extends Fragment {

    com.pkmmte.view.CircularImageView imageView;
    LinearLayout password_layout;
    EditText password;
    EditText re_password;
    EditText user_name;
    EditText user_phone_number;
    EditText user_email;
    LinearLayout only_docotor_layout=null;
    AutoCompleteTextView specialization=null;
    AutoCompleteTextView clinic_name_field=null;
    EditText cnic=null;
    EditText pmdc=null;
    EditText experinece=null;
    EditText clinic_contact_feild=null;
    static  Context context;

    View view;

    ActionButton location_button;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =inflater.inflate(R.layout.fragment_detail_info_fragment_doctor, container, false);


        imageView= (com.pkmmte.view.CircularImageView) view.findViewById(R.id.profilePic);


        Picasso.with(getActivity()).load(Information.selected_doctor.getProfile_photo()).placeholder(R.drawable.de_profile_image).into(imageView);



        user_name=(EditText) view.findViewById(R.id.user_name);
        user_name.setText(Information.selected_doctor.getUser_name());
        user_phone_number=(EditText) view.findViewById(R.id.user_phone_number);
        user_phone_number.setText(Information.selected_doctor.getUsers_phone_number());
        user_email=(EditText) view.findViewById(R.id.user_email);
        user_email.setText(Information.selected_doctor.getUsers_email_id());
        only_docotor_layout=(LinearLayout) view.findViewById(R.id.only_docotor_layout);
        only_docotor_layout.setVisibility(View.VISIBLE);
        specialization= (AutoCompleteTextView) view.findViewById(R.id.specilization);
        specialization.setText(Information.selected_doctor.getSpecialization());
        clinic_name_field=(AutoCompleteTextView) view.findViewById(R.id.Clinic_Name);
        clinic_name_field.setText(Information.selected_doctor.getClinic_name());


        cnic=(EditText)  view.findViewById(R.id.CNIC);
        cnic.setText(Information.selected_doctor.getCnic());
        pmdc=(EditText)  view.findViewById(R.id.PMDC);
        pmdc.setText(Information.selected_doctor.getPmdc());
        //Experinece
        experinece=(EditText) view.findViewById(R.id.experience);
        experinece.setText(Information.selected_doctor.getExperience());

//clinic contact
        clinic_contact_feild=(EditText)  view.findViewById(R.id.Clinic_ContactNo);
        clinic_contact_feild.setText(Information.selected_doctor.getClnic_number());
//gender
        final RadioRealButtonGroup radiogroup= (RadioRealButtonGroup)view.findViewById(R.id.gender);
        final RadioRealButtonGroup radiogroup_1= (RadioRealButtonGroup)view.findViewById(R.id.proffesion);


        return  view;
    }
}
