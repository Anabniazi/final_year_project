package com.example.hp_laptop.final_year_project;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import io.saeid.fabloading.LoadingView;


/**
 * Created by Office-HPR on 3/27/2017.
 */

public class Dailogs {
    static Dialog dialog;
    static LoadingView mLoadingView = null;
    public static void Toast(Context con, String Msg){

        Toast.makeText(con,Msg,Toast.LENGTH_SHORT).show();

    }
    public static Drawable getDrawableFromRes(int ResID, Context context) {

        if (Build.VERSION.SDK_INT >= 21)  context.getDrawable(ResID);
        return context.getResources().getDrawable((ResID));
    }

    public static void Loading(Context context) {


        if(dialog!=null)
        {
            DismissLoading(context);
        }


        dialog = new Dialog(context);
        dialog.setContentView(R.layout.layout_loading);
        dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        dialog.setCancelable(false);
//        ImageView mLoadingView = (ImageView) dialog.findViewById(R.id.loading_view);
//        Glide.with(context).load(R.raw.loader_new).into(new GlideDrawableImageViewTarget(mLoadingView));

//        mLoadingView = (LoadingView) dialog.findViewById(R.id.loading_view);
//        boolean isLollipop = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
//        int marvel_1 = isLollipop ? R.drawable.loading1 : R.drawable.loading1;
//        int marvel_2 = isLollipop ? R.drawable.loading2 : R.drawable.loading2;
//        int marvel_3 = isLollipop ? R.drawable.loading3 : R.drawable.loading3;
//        int marvel_4 = isLollipop ? R.drawable.loading4 : R.drawable.loading4;
//        mLoadingView.setDuration(250);
//        mLoadingView.addAnimation(Color.parseColor("#00FFD200"), marvel_1, LoadingView.FROM_LEFT);
//        mLoadingView.addAnimation(Color.parseColor("#00FFD200"), marvel_2, LoadingView.FROM_TOP);
//        mLoadingView.addAnimation(Color.parseColor("#00FFD200"), marvel_3, LoadingView.FROM_RIGHT);
//        mLoadingView.addAnimation(Color.parseColor("#00FFD200"), marvel_4, LoadingView.FROM_BOTTOM);
//
//
//        mLoadingView.startAnimation();
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(dialog.getWindow().getAttributes());
//        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        if (dialog!=null && !dialog.isShowing())
        {
            dialog.show();
        }

      //  dialog.getWindow().setAttributes(lp);

    }

    public static void DismissLoading(Context context) {
        try{
          //  mLoadingView.pauseAnimation();
            dialog.dismiss();
        }catch (Exception ex){
            Toast.makeText(context,"cant dismiss",Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

   /* public static void DismissLoading() {
        try{
            //  mLoadingView.pauseAnimation();
            dialog.dismiss();
        }catch (Exception ex){
            Toast.makeText(Context,"cant dismiss",Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }*/



    public static void showAlertDialog(String warning, final Context context) {

        AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);

        alertdialog.setMessage(warning);
        alertdialog.setCancelable(false);

        alertdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Activity activity =(Activity)context;
                activity.finish();

            }
        });

        alertdialog.show();

    }
//    public static void showOrderConfrimDialog(String warning, final Context context) {
//
//        AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);
//        alertdialog.setCancelable(false);
//        alertdialog.setMessage(warning);
//
//        alertdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                //((Activity) context).finish();
//                Intent myIntent = new Intent(((Activity) context), OrderHistroy.class);
//                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                ((Activity) context).startActivity(myIntent);
//
//            }
//        });
//
//        alertdialog.show();
//
//    }


    public static void showDialog(String warning, final Context context) {

        AlertDialog.Builder alertdialog = new AlertDialog.Builder(context);

        alertdialog.setMessage(warning);

        alertdialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();


            }
        });

        alertdialog.show();

    }

}
