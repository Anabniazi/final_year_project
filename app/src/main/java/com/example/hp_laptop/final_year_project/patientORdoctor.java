package com.example.hp_laptop.final_year_project;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import java.util.zip.Inflater;

public class patientORdoctor extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.patientordoctor,container,false);
        RadioGroup radiogroup= (RadioGroup) view.findViewById(R.id.doctor_or_patient);
        Button b1=(Button) view.findViewById(R.id.Ok_Button);
        Button b2=(Button)  view.findViewById(R.id.cancel_Button);
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

       radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(RadioGroup radioGroup, int i) {
               switch (i){

                   case R.id.doctor:
                       Information.doctor_or_patient="doctor";
                       break;
                   case  R.id.patient:
                       Information.doctor_or_patient="patient";
                       break;
                       }
                       }
       });
       b1.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {


             if(Information.doctor_or_patient.equals("doctor")){
                 Context c = getActivity();
                 Intent i =new Intent(c,Doctor_Profile.class);
                 c.startActivity(i);


                 }

                else if(Information.doctor_or_patient.equals("patient")){
                 Context c = getActivity();
                 Intent i = new Intent(c,Patient_Profile.class);
                 c.startActivity(i);




             }
               }
       });



       return view;


    }


}
