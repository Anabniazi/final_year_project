package com.example.hp_laptop.final_year_project;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import java.io.IOException;

public class Signin_user_detailed_info extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin_user_detailed_info);

        de.hdodenhof.circleimageview.CircleImageView profileimage=(de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_profile_pic);
        final EditText t1=(EditText)findViewById(R.id.user_name);
        final EditText t2=(EditText)findViewById(R.id.user_email);
        final EditText t3=(EditText)findViewById(R.id.user_phone_number);

        RadioGroup radiogroup=(RadioGroup)  findViewById(R.id.doctor_or_patient);
        Button doctor=(Button) findViewById(R.id.doctor);
        Button patient=(Button) findViewById(R.id.patient);

        final EditText t4 =(EditText) findViewById(R.id.specilization);
       final  EditText t5=(EditText)  findViewById(R.id.hospital_Name);
        final EditText t6=(EditText)  findViewById(R.id.Clinic_Name);
        final EditText t7=(EditText)   findViewById(R.id.Hospital_Address);
        final EditText t8=(EditText)  findViewById(R.id.Hospital_ContactNo);
        final EditText t9=(EditText)  findViewById(R.id.clinic_address);
        final EditText t10=(EditText)  findViewById(R.id.Clinic_ContactNo);


        t1.setText(Information.user_name);
        t2.setText(Information.users_email_id);
        t3.setText(Information.users_phone_number);

        Picasso.with(Signin_user_detailed_info.this).load(Information.users_image).placeholder(R.drawable.de_profile_image).into(profileimage);

        profileimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"select files"),456);
            }
        });
        Button b1=(Button) findViewById(R.id.Ok_Button);
        Button b2=(Button) findViewById(R.id.Cancell_Button);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s1=t1.getText()+"";
                String s2=t2.getText()+"";
                String s3=t3.getText()+"";

                String s4=t4.getText()+"";
                String s5=t5.getText()+"";
                String s6=t6.getText()+"";

                String s7=t7.getText()+"";
                String s8=t8.getText()+"";
                String s9=t9.getText()+"";

                String s10=t10.getText()+"";


            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AuthUI.getInstance()
                        .signOut(Signin_user_detailed_info.this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {


                                Intent i = new Intent(Signin_user_detailed_info.this,MainActivity.class);
                                startActivity(i);

                            }
                        });


            }
        });


        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                  @Override
                                                  public void onCheckedChanged(RadioGroup radioGroup, int i) {
                                                      switch (i){
                                                      case R.id.doctor:

                                                          break;
                                                      case R.id.patient:

                                                              break;

                                                  }}
                                              }
        );
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            Bitmap pic= MediaStore.Images.Media.getBitmap(this.getContentResolver(),data.getData());
            de.hdodenhof.circleimageview.CircleImageView profileimage=(de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_profile_pic);
            profileimage.setImageBitmap(pic);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
