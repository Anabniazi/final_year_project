package com.example.hp_laptop.final_year_project;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.ui.IconGenerator;
import com.scalified.fab.ActionButton;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.example.hp_laptop.final_year_project.Dailogs.DismissLoading;

public class Home_fragment extends Fragment implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener{
    View v;
    private MapFragment m;
    private static GoogleMap mMap = null;
    FusedLocationProviderClient fusedLocationProviderClient;
    String city;
    public static Context context;
    private ActionButton logout_button;
    PlaceAutocompleteFragment autocompleteFragment;
    private GoogleApiClient mGoogleApiClient;
    private String selected_city="";
    private String selected_bloodgroup="";
    AutoCompleteTextView autocomplete;
    List<String> Blood_group = Arrays.asList("A+","B+","AB+","A-","B-","AB-","O+","O-");
    com.github.pavlospt.CircleView blood_group_txt;
    ActionButton history_button;
    ActionButton notification_button;
    FusedLocationProviderClient mFusedLocationClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        context=getActivity();

        if (v != null) {

            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null) {

                parent.removeView(v);
            }

        }


        try {

            v = inflater.inflate(R.layout.fragment_home_fragment, container, false);
            notification_button=(ActionButton)v.findViewById(R.id.action_button);
            notification_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {

                    Intent intent = new Intent(getActivity(),pateints_options.class);
                    getActivity().startActivity(intent);

                    /*Pateint_notifications_fragment Appointment_fragment = new Pateint_notifications_fragment();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.placeholder, Appointment_fragment,"findThisFragment")
                            .addToBackStack(null)
                            .commit();*/

                }
            });

        } catch (InflateException e) {

            Log.e("MYAPP", "exception", e);
            /* map is already there, just return view as it is */
        }





        /*logout_button=(ActionButton)v.findViewById(R.id.logout_button);
        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(autocompleteFragment != null && getActivity() != null && !getActivity().isFinishing())
                {
                    getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment).commit();
                }

              //  destory_map();
                FirebaseAuth.getInstance().signOut();
                getActivity().finish();
                Intent intent=new Intent(context,MainActivity.class);
                context.startActivity(intent);
            }
        });

        history_button=(ActionButton)v.findViewById(R.id.history_button);
        history_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(autocompleteFragment != null && getActivity() != null && !getActivity().isFinishing()) {
                    getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment).commit();
                }


               // destory_map();
                Intent intent = new Intent(getActivity(),History_Activity.class);
                getActivity().startActivity(intent);
            }
        });
        notification_button=(ActionButton)v.findViewById(R.id.notification_button);
        notification_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(autocompleteFragment != null && getActivity() != null && !getActivity().isFinishing()) {
                    getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment).commit();
                }

              //  destory_map();
                Intent intent = new Intent(getActivity(),Notification_detail_Activity.class);
                intent.putExtra("reciever_email",Login_User_Info.email);
                getActivity().startActivity(intent);
            }
        });
*/

        autocomplete = (AutoCompleteTextView) v.findViewById(R.id.blood_autocompleteadapter);
        autocomplete.setThreshold(0); //ya pochna

        Blood_group=Arrays.asList(getContext().getResources().getStringArray(R.array.specilizations));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.select_dialog_item, Blood_group);
        autocomplete.setAdapter(adapter);

        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {


                selected_bloodgroup = parent.getItemAtPosition(pos).toString();
                Toast.makeText(getActivity(),selected_bloodgroup, Toast.LENGTH_LONG).show();

                if(!selected_bloodgroup.equals("") && !selected_city.equals(""))
                {


                    show_donors_and_bloodbaks_selected();
                }




            }
        });





        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());


        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(new user_sigin_location(), this)
                .build();





        // google auto complete fragent

         autocompleteFragment = (PlaceAutocompleteFragment)
                getActivity().getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                mMap.clear();
                city = Geocoding(place.getLatLng().latitude+"",place.getLatLng().longitude+"");




                selected_city=city;

                if(!selected_bloodgroup.equals("") && !selected_city.equals(""))
                {
                    show_donors_and_bloodbaks_selected();
                }
               Toast.makeText(getActivity(),city,Toast.LENGTH_LONG).show();



               // show_marker(place.getLatLng());


            }

            @Override
            public void onError(Status status) {

            }
        });






        show_donors_and_bloodbaks();


        return v;




    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap=googleMap;

        mMap.setOnMarkerClickListener(markerlistener);

      //  show_users(getActivity());


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        final SupportMapFragment myMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googlemap);
        myMapFragment.getMapAsync(this);


        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();

        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        if((((AppCompatActivity) getActivity()).getSupportActionBar())!=null)
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        mGoogleApiClient.connect();


    }

    public String Geocoding(String latitude,String longitutde)
    {
        Geocoder gcd = new Geocoder(getContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitutde), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {


            Toast.makeText(getActivity(),"current_city "+addresses.get(0).getLocality(),Toast.LENGTH_LONG).show();
            return addresses.get(0).getLocality();
        }
        else {
            // do your stuff
        }
        return  "";
    }


    public static void show_marker(LatLng latlang,Context context)
    {
        if(mMap==null)
        {
            return;
        }

        IconGenerator generator = new IconGenerator(context);
        MarkerOptions markerOption = new MarkerOptions();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View markerView = inflater.inflate(R.layout.info_window, null);

        com.github.pavlospt.CircleView blood_group_txt;

        blood_group_txt =(com.github.pavlospt.CircleView) markerView.findViewById(R.id.blood_group_txt);
       // blood_diner_image=(de.hdodenhof.circleimageview.CircleImageView) markerView.findViewById(R.id.blood_diner_image);


        blood_group_txt.setTitleText("D");



        generator.setContentView(markerView);
        generator.setBackground(null);
        //MapInitializer.initialize()

        Bitmap icon = generator.makeIcon();
        markerOption.icon(BitmapDescriptorFactory.fromBitmap(icon))
                .position(latlang)
                .anchor(generator.getAnchorU(), generator.getAnchorV());
        mMap.addMarker(markerOption);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlang));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));




    }

    public static void show_users(Context context)
    {


        if(Information.same_city_users.size()>0)
        {
            for(int i=0;i<Information.same_city_users.size();i++)
            {

                Log.d("user_number",i+"  -user");
                show_marker(new LatLng(Double.parseDouble(Information.same_city_users.get(i).getLatitude()),Double.parseDouble(Information.same_city_users.get(i).getLongitutude())),context);
            }
        }
    }


    GoogleMap.OnMarkerClickListener markerlistener = new GoogleMap.OnMarkerClickListener() {
        @Override
        public boolean onMarkerClick(Marker marker)
        {

            for(int i=0;i<Information.same_city_users.size();i++)
            {
                if(Information.same_city_users.get(i).getLatitude().equals(marker.getPosition().latitude+"") && Information.same_city_users.get(i).getLongitutude().equals(marker.getPosition().longitude+""))
                {

                    Information.selected_doctor=Information.same_city_users.get(i);

                    Intent intent = new Intent(getActivity(),Patients_option_for_doctor.class);
                    getActivity().startActivity(intent);


                    /*Detail_info_fragment_doctor nextFrag= new Detail_info_fragment_doctor();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.placeholder, nextFrag,"findThisFragment")
                            .addToBackStack(null)
                            .commit();*/
                }
            }

            return false;
        }
    };



    private void show_donors_and_bloodbaks()
    {

        if(Information.same_city_users!=null)
        {

            Information.same_city_users.clear();
        }

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("Users");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot
                        ArrayList<User> arruser = new ArrayList<>();

                        for (DataSnapshot dsp : dataSnapshot.getChildren())
                        {
                            User user = dsp.getValue(User.class);


                            if(user.getDoctor_or_patient().equals("doctor") && user.getCity().equals(Information.loged_in_user.getCity()))
                            {
                                Information.same_city_users.add(user);
                            }

                        }

                        Home_fragment.show_users(context);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });



    }


    private void show_donors_and_bloodbaks_selected()
    {

        if(Information.same_city_users!=null)
        {

            Information.same_city_users.clear();
        }

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("online_doctor").child("Users");
        ref.addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //Get map of users in datasnapshot
                        ArrayList<User> arruser = new ArrayList<>();

                        for (DataSnapshot dsp : dataSnapshot.getChildren())
                        {
                            User user = dsp.getValue(User.class);


                            if(user.getDoctor_or_patient().equals("doctor") && user.getCity().equals(selected_city) && user.getSpecialization().equals(selected_bloodgroup))
                            {
                                Information.same_city_users.add(user);
                            }

                        }

                        Home_fragment.show_users(context);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });



    }
}
