package io.realm;


public interface com_example_hp_laptop_final_year_project_models_LogCallRealmProxyInterface {
    public com.example.hp_laptop.final_year_project.models.User realmGet$user();
    public void realmSet$user(com.example.hp_laptop.final_year_project.models.User value);
    public long realmGet$timeUpdated();
    public void realmSet$timeUpdated(long value);
    public int realmGet$timeDurationSeconds();
    public void realmSet$timeDurationSeconds(int value);
    public String realmGet$status();
    public void realmSet$status(String value);
    public String realmGet$myId();
    public void realmSet$myId(String value);
    public String realmGet$userId();
    public void realmSet$userId(String value);
    public boolean realmGet$isVideo();
    public void realmSet$isVideo(boolean value);
}
