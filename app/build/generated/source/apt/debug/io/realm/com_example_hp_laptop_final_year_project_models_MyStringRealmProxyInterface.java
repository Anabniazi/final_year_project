package io.realm;


public interface com_example_hp_laptop_final_year_project_models_MyStringRealmProxyInterface {
    public String realmGet$string();
    public void realmSet$string(String value);
}
