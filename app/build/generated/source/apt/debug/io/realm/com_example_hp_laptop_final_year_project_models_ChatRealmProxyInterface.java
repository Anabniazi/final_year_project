package io.realm;


public interface com_example_hp_laptop_final_year_project_models_ChatRealmProxyInterface {
    public RealmList<com.example.hp_laptop.final_year_project.models.Message> realmGet$messages();
    public void realmSet$messages(RealmList<com.example.hp_laptop.final_year_project.models.Message> value);
    public String realmGet$lastMessage();
    public void realmSet$lastMessage(String value);
    public String realmGet$myId();
    public void realmSet$myId(String value);
    public String realmGet$userId();
    public void realmSet$userId(String value);
    public String realmGet$groupId();
    public void realmSet$groupId(String value);
    public long realmGet$timeUpdated();
    public void realmSet$timeUpdated(long value);
    public com.example.hp_laptop.final_year_project.models.User realmGet$user();
    public void realmSet$user(com.example.hp_laptop.final_year_project.models.User value);
    public com.example.hp_laptop.final_year_project.models.Group realmGet$group();
    public void realmSet$group(com.example.hp_laptop.final_year_project.models.Group value);
    public boolean realmGet$read();
    public void realmSet$read(boolean value);
}
