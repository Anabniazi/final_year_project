package io.realm;


public interface com_example_hp_laptop_final_year_project_models_AttachmentRealmProxyInterface {
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$data();
    public void realmSet$data(String value);
    public String realmGet$url();
    public void realmSet$url(String value);
    public long realmGet$bytesCount();
    public void realmSet$bytesCount(long value);
}
