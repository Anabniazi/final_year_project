package io.realm;


public interface com_example_hp_laptop_final_year_project_models_GroupRealmProxyInterface {
    public String realmGet$id();
    public void realmSet$id(String value);
    public String realmGet$name();
    public void realmSet$name(String value);
    public String realmGet$status();
    public void realmSet$status(String value);
    public String realmGet$image();
    public void realmSet$image(String value);
    public RealmList<com.example.hp_laptop.final_year_project.models.MyString> realmGet$userIds();
    public void realmSet$userIds(RealmList<com.example.hp_laptop.final_year_project.models.MyString> value);
}
